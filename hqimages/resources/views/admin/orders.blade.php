@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Order Manager!
        </p>
    @endif
    <div class="simple-text"><h3>Order Manager</h3></div>
    <div class="row" align="center">

        
<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">

                <thead>
                    
                    <th>ID</th>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Edit</th>

                </thead>
                <tfoot>
                     
                    <th>ID</th>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Edit</th>

                </tfoot>
                <tbody>
                    
                @foreach($orderalls as $orderall)
                <tr>
                   
                   
                        
                        <td>{{$orderall->id}}</td>
                        <td>{{$orderall->account}}</td>
                        <td>{{$orderall->name}}</td>
                        <td>{{$orderall->address}}</td>
                        <td>{{$orderall->phone}}</td>
                        <td>{{$orderall->price}}</td>
                        <td>{{$orderall->stat_name}}</td>
                           
                            <td>

                                {!!  Form::open(['method' => 'GET', 'route' => ['admin.orders.edit',$orderall->id]])  !!}
                                {!!   Form::submit('Edit', ['class' => 'btn btn-info']) !!}
                                {!!  Form::close() !!}
                            </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>


@endsection