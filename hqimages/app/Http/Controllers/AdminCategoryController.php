<?php

namespace App\Http\Controllers;
use App\Category;
use App\Http\Requests\AdminCategoryRequest;
use App\User;
use Illuminate\Http\Request;

class AdminCategoryController extends Controller {
	public function index(Request $request) {
		$categ = Category::orderBy('id', 'desc')->get();
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		return view('admin.categories', ['admin_infos' => $admin_infos, 'categs' => $categ])->with('message', 'Welcome to Category!');
	}
	public function destroy(Request $request, $id) {
		$user = Category::findOrFail($id);
		$user->delete();
		return redirect('admin/categories')->with('message', 'Delete success!');
	}
	public function create(Request $request) {

		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		return view('admin.create_category', ['admin_infos' => $admin_infos])->with('message', 'Please type info!');
	}
	public function store(AdminCategoryRequest $request) {
		$data = $request->all();
		$categ = new Category;
		$categ->categ_name = $data['name'];

		$categ->save();
		return redirect('admin/categories')->with('message', 'Add new categ success!');

	}
	public function edit(Request $request, $id) {
		$categ = Category::find($id);
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		return view('admin.edit_categ', ['admin_infos' => $admin_infos, 'categ' => $categ])->with('message', 'Please update!');
	}
	public function update(AdminCategoryRequest $request, $id) {
		$categ = Category::find($id);
		$data = $request->all();
		$categ->categ_name = $data['name'];

		$categ->save();
		return redirect('admin/categories')->with('message', 'Update success!');
	}
}
