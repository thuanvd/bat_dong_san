@extends('layouts.adminlayouts')
@section('content')
    <div class="simple-text"><h3>User Management</h3></div>
    {!!  Form::open(['method' => 'GET', 'route' => ['admin.users.create']])  !!}
    {!!   Form::submit('Create New User', ['class' => 'btn btn-warning']) !!}
    {!!  Form::close() !!}
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
        @else
        <p id="mess" style="display: none">
            Welcome User Management!
        </p>
    @endif
    <br>
    <div class="row" align="center">


<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <th>ID</th>
            <th>NAME</th>
            <th>ACCOUNT</th>
            <th>PASSWORD</th>
            <th>EMAIL</th>
            <th>STATUS</th>
            <th>ROLE</th>
            <th>EDIT</th>
        </thead>
        <tfoot>
            <th>ID</th>
            <th>NAME</th>
            <th>ACCOUNT</th>
            <th>PASSWORD</th>
            <th>EMAIL</th>
            <th>STATUS</th>
            <th>ROLE</th>
            <th>EDIT</th>
        </tfoot>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->account}}</td>
            <td><span type="password" style="-webkit-text-security: square;">{{$user->password}}</span></td>
            <td>{{$user->email}}</td>
            <td>{{$user->status_name}}</td>
            <td>{{$user->role_name}}</td>
            <td>
                {!!  Form::open(['method' => 'GET', 'route' => ['admin.users.edit',$user->id]])  !!}
                {!!   Form::submit('Edit', ['class' => 'btn btn-info']) !!}
                {!!  Form::close() !!}
                </td>
        </tr>
            @endforeach
            </tbody>
    </table>
    </div>
      {{-- type="password" style="-webkit-text-security: square;" --}}
{{-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> --}}

@endsection