<?php
//use Illuminate\Routing\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

//use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
// 	return view('common.home');
// });
//use Illuminate\Routing\Route;

Route::get('/', 'HomeController@getIndex');
Route::get('categ/{id}', 'CategoryController@getIndex');
Route::get('product/{id_pro}', 'ProductController@getIndex');

// Route::post('login', 'UsersController@postLogin');
Route::controller('user', 'UsersController', ['getSignin' => 'signin', 'postSignin' => 'signin', 'getSignup' => 'signup', 'postSignup' => 'signup']);

Route::get('update/{id}', 'UserinfoController@getUpdate')->middleware('auth');
Route::post('update/{id}', 'UserinfoController@postUpdate')->middleware('auth');
Route::get('changepass', 'UserinfoController@getChangepass')->middleware('auth');
Route::post('changepass/{id}', 'UserinfoController@postChangepass')->middleware('auth');
Route::post('favorite/{id}', 'UserinfoController@postAddfavorite');
Route::post('addcart/{id}', 'UserinfoController@postAddcart');
Route::get('carts', 'UserinfoController@carts')->middleware('auth');
Route::get('favorites', 'UserinfoController@favorites')->middleware('auth');
Route::get('favorite/delete/{id}', 'UserinfoController@delfavorites');
Route::get('cart/delete/{id}', 'UserinfoController@delcart');

Route::get('admin', 'AdminController@index');
Route::resource('admin/users', 'AdminUserController');
Route::resource('admin/sliders', 'AdminSliderController');
Route::resource('admin/products', 'AdminProductController');
Route::resource('admin/categories', 'AdminCategoryController');
Route::resource('admin/orders', 'AdminOrderController');
Route::get('admin/orders/details/{id}', ['as' => 'admin.orders.details', 'uses' => 'AdminOrderController@orderdetail']);
Route::delete('admin/orders/details/destroy/{id}', ['as' => 'admin.orders.details.destroy', 'uses' => 'AdminOrderController@orderdetaildestroy']);

Route::get('download/{id}/{path}', 'PaymentController@download');

Route::get('about-me', function () {
	return 'Giới thiệu';
});
Route::get('contact-me', function () {
	return 'Liên hệ';
});
Route::post('/search/{query}', 'SearchController@searchproduct');
//-------------------------------------------------------------
Route::post('search', 'SearchController@searchimage');
Route::get('order', 'OrderController@index');
Route::post('payment', 'PaymentController@index');
Route::post('update-payment/{stat}/{id}', 'PaymentController@updatepayment');

Route::get('confirm', 'PaymentController@getConfirm');
Route::get('bill', 'PaymentController@billindex');
Route::get('bill/detail/{stat}/{id}', 'PaymentController@billdetail');
