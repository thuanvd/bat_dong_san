@extends('layouts.layouts')
@section('breadcrumb')
	<li><a href="{{Asset('/')}}">Home</a></li>

@endsection
@section('content')

    <div class="row">
    <div class="col-md-1"></div>
     <div class="col-md-8">
     @foreach($products as $pro)
	  <div class="col-lg-4">
	  <div class="hovereffect">
	    <div class="thumbnail">
    	<a href="{{Asset('product')}}/{{$pro->id}}"><img src="{{Asset('thumbnail')}}/{{$pro->path}}" style="width:100%; height: 250px"></a>
	     {{--  <div class="caption"> --}}


	        <div class="overlay">
           <h2>{{$pro->product_name}}</h2>
           <a class="info" href="{{Asset('product')}}/{{$pro->id}}">link here</a>
       		 </div>
       		  </div>
       		 </div>
	        <p align="center"><a href="#" class="btn btn-success" role="button" id="{{ $pro->id }}" name="addcart">
	        <span class="glyphicon glyphicon-shopping-cart"></span> Cart</a>

	         <a href="{{Asset('favorites')}}" class="btn btn-danger" role="button"  id="{{ $pro->id }}" name="favorite">
	         <span class="glyphicon glyphicon-heart"></span> Favorite</a>

	         <br><span class="label label-warning" style="background-color: black">Download: {{$pro->download}}</span>
	         	 <span class="label label-warning" id="{{$pro->id}}" style="background-color: orange">Views: {{$pro->views}}</span></p>
	     {{--  </div> --}}
	  </div>
	  @endforeach
	  <div class="row">
  <div class="col-md-5"></div>
  <div class="col-md-3">{{$products->links()}}</div>
  <div class="col-md-4"></div>
</div>
	  </div>
	  <div class="col-md-2">
	  	<div class="panel panel-success">
			<div class="panel-heading">
			    <h3 class="panel-title">Top Download</h3>
			  </div>
			  <div class="panel-body">
			    @foreach($tops as $top)
			    <a href="{{Asset('product')}}/{{$top->id}}"><img src="{{Asset('images')}}/{{$top->path}}" style="width:250px; height: 150px;" class="img image-4"></a>
			    <span class="label label-warning" style="background-color: green">Download: {{$top->download}}</span></p>
			    <a href="{{Asset('product')}}/{{$top->id}}"><h4 style="text-align: center">{{$top->product_name}}</h4></a>
			    @endforeach
			  </div>
			</div>
		 	</div>
	</div>

@endsection