<?php
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller {

	public function getIndex(Request $request) {

		$categs = DB::table('product_categs')->get();
		$products = DB::table('products')->orderBy('id','desc')->paginate(9);
		$tops = DB::table('products')->orderBy('views', 'desc')->take(7)->get();
		return view("common.home", ['tops' => $tops])->with('categs', $categs)
			->with('products', $products);
	}
}
