@extends('layouts.layouts')
@section('breadcrumb')
        <li><a href="{{Asset('/')}}">Home</a></li>
        <li class=""><a href="{{Asset('update/')}}/{{Session::get('id')}}">User</a></li>
        <li class="active">Bill</li>
@endsection
@section('content')
    <div class="row" style="min-height: 300px;">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <div class="panel panel-info" >
                <!-- Default panel contents -->
                <div class="panel-heading" align="center"><h4>Your Bill Unconfirm</h4></div>

            @if (session('payment_success'))
                <div class="alert alert-info" align="center">
                    {{ session('payment_success') }}
                </div>
            @endif
                @if (session('update_suc'))
                    <div class="alert alert-success" align="center">
                        {{ session('update_suc') }}
                    </div>
                @endif

            <table class="table" id="example">
                <thead>
                    <tr>
                        <th>#</th>
                        <th style="display: none">ID</th>
                        <th>Price</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Adress</th>
                        <th>Phone no.</th>
                         <th>Status</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                @if (session('no_uncon'))
                    <div class="alert alert-warning" align="center">
                        {{ session('no_uncon') }}
                    </div>
                @endif
                <?php $stt = 1;?>
                @foreach ($unconfirms as $unconfirm)
                    {{-- expr --}}

                    <tr>
                        <td>{{ $stt }}</td>
                        <td style="display: none">{{ $unconfirm->id }}</td>
                        <td>{{  $unconfirm->price}}</td>
                        <td>{{  $unconfirm->created_at }}</td>
                        <td>{{  $unconfirm->name }}</td>
                        <td>{{  $unconfirm->address }}</td>
                        <td>{{  $unconfirm->phone }}</td>
                         <td>{{  $unconfirm->stat_name }}</td>
                        <td> <a href="{{Asset('bill/detail')}}/{{ $unconfirm->stat_id }}/{{ $unconfirm->id }}"><button class="btn btn-warning " type="button">{{-- <span class="glyphicon glyphicon-arrow-left"></span> --}} Details</button></a></td>
                    </tr>
                    <?php $stt = $stt + 1;?>
                     @endforeach
                </tbody>

            </table>

            </div>

             <div class="panel panel-info" >
                <!-- Default panel contents -->
                <div class="panel-heading" align="center"><h4>Your Bill Confirm</h4></div>

            @if (session('payment_success'))
                <div class="alert alert-info" align="center">
                    {{ session('payment_success') }}
                </div>
            @endif

            <table class="table display" id='example'>

                <thead>
                    <tr>
                        <th>#</th>
                        <th style="display: none">ID</th>
                        <th>Price</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Adress</th>
                        <th>Phone no.</th>
                          <th>Status</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                @if (session('no_con'))
                    <div class="alert alert-warning" align="center">
                        {{ session('no_con') }}
                    </div>
                @endif
                <?php $stt = 1;?>
                @foreach ($confirms as $confirm)
                    {{-- expr --}}

                    <tr>
                        <td>{{ $stt }}</td>
                        <td style="display: none">{{ $confirm->id }}</td>
                        <td>{{  $confirm->price}}</td>
                        <td>{{  $confirm->created_at }}</td>
                        <td>{{  $confirm->name }}</td>
                        <td>{{  $confirm->address }}</td>
                        <td>{{  $confirm->phone }}</td>
                          <td>{{  $confirm->stat_name }}</td>
                        <td> <a href="{{Asset('bill/detail')}}/{{  $confirm->stat_id }}/{{ $confirm->id }}"><button class="btn btn-warning " type="button">{{-- <span class="glyphicon glyphicon-arrow-left"></span> --}} Details</button></a></td>
                    </tr>
                    <?php $stt = $stt + 1;?>
                     @endforeach
                </tbody>

            </table>

            </div>

            <p style="text-align:left;">
                <a href="{{Asset('/')}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a>

            </p>
        </div>
    </div>



    <div class="col-md-2">

    </div>

<script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#example').DataTable();
            } );
        </script>
<script type="text/javascript">
    // For demo to fit into DataTables site builder...
    $('.table')
        .removeClass( 'display' )
        .addClass('table table-striped table-bordered');
</script>

@endsection