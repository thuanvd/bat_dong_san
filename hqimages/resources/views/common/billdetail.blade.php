@extends('layouts.layouts')
@section('breadcrumb')
@section('breadcrumb')
  <li><a href="{{Asset('/')}}">Home</a></li>
    <li class="active">Bill Detail</li>
@endsection
@section('content')
<div class="row">
<div class="col-md-4"></div>
  <div class="col-md-4" style="border: solid 1px;">


    <h3 >Bill Details</h3>
     @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif

        @if ( $stat == 1)
            @foreach($infoOrders as $infoOrder)
      <form method='post' action="{{Asset('update-payment')}}/{{$stat}}/{{$infoOrder->id}}" id="post_payment">
          <div class="form-group">
              <label for="exampleInputEmail1">Your name</label>
              <input type="text" class="form-control" id="name" name='name' placeholder="Name" value="{{$infoOrder->name}}" >
          </div>
          <div class="form-group">
              <label for="exampleInputPassword1">Address</label>
              <input type="text" class="form-control" id="adress" name='address' placeholder="Address"  value="{{$infoOrder->address}}">
          </div>
          <div class="form-group">
              <label for="exampleInputPassword1">Phone no.</label>
              <input type="number" class="form-control" id="phone" name='phone' placeholder="Phone number"  value="{{$infoOrder->phone}}">
          </div>

          <div >
              <button type="submit" id='payment' class="btn btn-success"></span>Update</button>
          </div>

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    </form>
        @endforeach
            @endif
        @if ($stat == 2)
        @foreach($infoOrders as $infoOrder)
            <table class="table">
                <thead>
                <tr>
                    <th><h3> Your info </h3></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{{$infoOrder->name}}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>{{$infoOrder->address}}</td>
                </tr>
                <tr>
                    <td>Phone no.</td>
                    <td>{{$infoOrder->phone}}</td>
                </tr>
                </tbody>

            </table>
        @endforeach
        @endif
   <br>
<hr>
  <h3>Your Product</h3>
<table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th style="display: none">ID</th>

                        <th>Name</th>
                        <th>Unit Price</th>
                        <th>Image</th>

                    </tr>
                </thead>
                <tbody>
                <?php $stt = 1;?>
                @foreach ($order_details as $order_detail)
                    {{-- expr --}}

                    <tr>
                        <td>{{ $stt }}</td>
                        <td style="display: none">{{ $order_detail->id }}</td>

                        <td>{{  $order_detail->product_name}}</td>
                        <td>{{  $order_detail->unit_price }}</td>
                        <td><img src="{{ Asset('thumbnail') }}/{{  $order_detail->path }}" style="width: 100px; height: 100px;" alt=""></td>

                        @if ( $order_detail->stat_id == 2)
                          <td> <a href="{{Asset('download')}}/{{ $order_detail->product_id }}/{{$order_detail->path}} "  target="_blank"><button class="btn btn-warning " type="button">{{-- <span class="glyphicon glyphicon-arrow-left"></span> --}} Download</button></a></td>
                        @endif


                    </tr>
                    <?php $stt = $stt + 1;?>
                     @endforeach
                </tbody>

            </table>


			<p style="text-align:left;"><a href="{{ URL::previous()}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a></p>

		 	</div>
  </div>
</div>


<div class="col-md-4">

</div>

  </div>
<script>
    $(document).ready(function(){
        $("#post_payment").submit(function(e){
            if(confirm('Are you sure?')){

                return true;
            }
            e.preventDefault();
            return false;
        });
    });
</script>
@endsection