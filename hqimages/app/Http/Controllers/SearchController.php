<?php

namespace App\Http\Controllers;
use App\Product;
use App\Product_categ;

class SearchController extends Controller {
	public function searchproduct($query) {
		$data = Product::searchimage($query);
		$data2 = Product_categ::searchcateg($query);
		return view('common.searchbox')->with('data', $data)->with('data2', $data2);
	}
}
