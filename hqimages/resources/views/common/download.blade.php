<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Download images</title>
	<link rel="stylesheet" href="">
     <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="row" align="center">
<h2>Click <b><i>Download button</i></b> to download </h2>
	<br>

  	<img src="{{Asset('images')}}/{{$path}}" alt="{{$path}}" >
		<br>

		<a href="{{Asset('images')}}/{{$path}}" download style="align:center"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-download"></span> Download Image File</button>
		</a>

  </div>




</body>
</html>