<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class User extends Model {
	protected $table = 'users';
	public static function getuserinfo() {
		return DB::table('users')
			->join('roles', 'roles.id', '=', 'users.role_id')
			->join('user_status', 'user_status.id', '=', 'users.status_id')
			->select('users.*', 'roles.role_name', 'user_status.status_name')
			->orderBy('users.id', 'desc')
			->get();
	}
	public function role() {
		return $this->hasMany('App\Role');
	}
}
