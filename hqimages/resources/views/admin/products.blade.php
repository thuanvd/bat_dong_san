@extends('layouts.adminlayouts')
@section('content')
    <div class="simple-text"><h3>Products Management</h3></div>
    {!!  Form::open(['method' => 'GET', 'route' => ['admin.products.create']])  !!}
    {!!   Form::submit('Create New Product', ['class' => 'btn btn-warning']) !!}
    {!!  Form::close() !!}
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Welcome Product Management!
        </p>
    @endif
    <br>
    <div class="row" align="center">          
<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                       
                            <th>ID</th>
            <th>Product Name</th>
            <th>Product Description</th>
            <th>Product Category</th>
            <th>Unit Price</th>
            <th>Downloads</th>
            <th>Views</th>
            <th>User Upload</th>
            <th>Image</th>

            <th>Delete</th>
            <th>Edit</th>
                        
                    </thead>

                    <tfoot>
                       
            <th>ID</th>
            <th>Product Name</th>
            <th>Product Description</th>
            <th>Product Category</th>
            <th>Unit Price</th>
            <th>Downloads</th>
            <th>Views</th>
            <th>User Upload</th>
            <th>Image</th>
            <th>Delete</th>
            <th>Edit</th>
                        
                    </tfoot>
                            <tbody>
                                  @foreach($products as $product)
            <tr>
                <td>{{$product->id}}</td>
                <td>{{$product->product_name}}</td>
                <td>{{$product->product_des}}</td>
                <td>{{$product->categ_name}}</td>
                <td>{{$product->unit_price}}</td>
                <td>{{$product->download}}</td>
                <td>{{$product->views}}</td>
                <td>{{$product->account}}</td>
                <td><image src="{{Asset('/images')}}/{{$product->path}}" style="height: 100px; width: 100px;"></image></td>
                <td >
                    {!!  Form::open(['method' => 'DELETE', 'route' => ['admin.products.destroy',$product->id]])  !!}
                    {!!   Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!!  Form::close() !!}
                </td>
                <td>
                    {!!  Form::open(['method' => 'GET', 'route' => ['admin.products.edit',$product->id]])  !!}
                    {!!   Form::submit('Edit', ['class' => 'btn btn-info']) !!}
                    {!!  Form::close() !!}
                </td>
            </tr>

        @endforeach
                            </tbody>
                    
                </table>


    </div>
  
   <script>
       $(document).ready(function(){
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('th').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});
   </script>
@endsection