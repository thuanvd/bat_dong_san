-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2016 at 12:06 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hqimages`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2012013_07_18_030432_create_roles_table', 1),
('2013_07_18_040147_create_user_status_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_18_015215_create_product_categs_table', 1),
('2016_07_18_015216_create_products_table', 1),
('2016_07_18_040146_create_favorites_table', 1),
('2016_07_18_040147_create_stats_table', 1),
('2016_07_18_040708_create_orders_table', 1),
('2016_07_18_040725_create_order_details_table', 1),
('2016_07_21_015058_carts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `stat_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_categ_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `download` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_des`, `product_categ_id`, `user_id`, `path`, `unit_price`, `download`, `views`, `created_at`, `updated_at`) VALUES
(15, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(16, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(17, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(18, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(19, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(20, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(21, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(22, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(23, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(24, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(25, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(26, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(27, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(28, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(29, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29'),
(30, 'adad', 'adawd', 3, 40, '13315505_732480120228627_1599781595698645869_n.jpg', '500000', 0, 4, '2016-08-11 00:02:29', '2016-08-11 00:02:29');

-- --------------------------------------------------------

--
-- Table structure for table `product_categs`
--

CREATE TABLE `product_categs` (
  `id` int(10) UNSIGNED NOT NULL,
  `categ_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_categs`
--

INSERT INTO `product_categs` (`id`, `categ_name`, `created_at`, `updated_at`) VALUES
(1, 'Phong Cảnh', '2016-07-20 11:59:42', '2016-08-10 23:37:44'),
(2, 'Frame', '2016-07-20 11:59:43', '2016-07-20 11:59:43'),
(3, 'Sticker', '2016-07-20 11:59:43', '2016-08-04 13:04:45'),
(4, 'Chân dung', '2016-07-20 11:59:43', '2016-07-20 11:59:43'),
(5, 'Thiên Thần', '2016-07-20 11:59:43', '2016-07-20 11:59:43'),
(10, 'Victoria Secret', '2016-08-04 12:58:16', '2016-08-04 12:58:16');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2016-07-20 11:57:22', '2016-07-20 11:57:22'),
(2, 'Staff', '2016-07-20 11:57:22', '2016-07-20 11:57:22'),
(3, 'User', '2016-07-20 11:57:23', '2016-07-20 11:57:23');

-- --------------------------------------------------------

--
-- Table structure for table `stats`
--

CREATE TABLE `stats` (
  `id` int(10) UNSIGNED NOT NULL,
  `stat_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stats`
--

INSERT INTO `stats` (`id`, `stat_name`, `created_at`, `updated_at`) VALUES
(1, 'Chưa xác nhận', '2016-08-02 03:00:00', '2016-08-02 03:00:00'),
(2, 'Xác nhận', '2016-08-02 03:00:00', '2016-08-02 03:00:00'),
(3, 'Hoàn thành', '2016-08-02 03:00:00', '2016-08-02 03:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `account`, `email`, `password`, `status_id`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(40, 'thuanvu', 'thuanvu', 'thuanvu@gmail.com', '123456', 1, 1, NULL, '2016-08-10 14:02:22', '2016-08-10 14:02:22'),
(42, 'thaovi', 'thaovi', 'thao@gmail.com', '123456', 2, 3, NULL, '2016-08-10 14:03:03', '2016-08-10 14:05:26'),
(43, 'mangcau', 'mangcau', 'mangcau@gmail.com', 'mangcau', 1, 3, NULL, '2016-08-10 14:09:30', '2016-08-10 14:09:30'),
(44, 'thanhlong12', 'thanhlong', 'thanhlong@gmail.com', 'thanhlong', 1, 2, NULL, '2016-08-10 14:09:46', '2016-08-11 00:51:01'),
(45, 'thuanvu1', 'thuanvu1', 'thuan1vu@gmail.com', '123456', 1, 1, NULL, '2016-08-10 14:02:22', '2016-08-10 14:02:22'),
(46, 'quanvu1', 'quanvu1', 'quan@gmail.com1', '123456', 1, 1, NULL, '2016-08-10 14:02:39', '2016-08-10 14:05:39'),
(47, 'thaovi1', 'thaovi1', 'thao@gmail.com1', '123456', 1, 3, NULL, '2016-08-10 14:03:03', '2016-08-10 14:05:26'),
(48, 'mangcau1', 'mangcau1', 'mangcau@gmail.com1', 'mangcau', 1, 3, NULL, '2016-08-10 14:09:30', '2016-08-10 14:09:30'),
(49, 'thanhlong1', 'thanhlong1', 'thanhlong@gmail.com1', 'thanhlong', 1, 3, NULL, '2016-08-10 14:09:46', '2016-08-10 14:09:46'),
(50, 'thuanvu2', 'thuanvu2', 'thuanvu@gmail.com2', '123456', 1, 1, NULL, '2016-08-10 14:02:22', '2016-08-10 14:02:22'),
(51, 'quanvu2', 'quanvu2', 'quan@gmail.com2', '123456', 1, 1, NULL, '2016-08-10 14:02:39', '2016-08-10 14:05:39'),
(52, 'thaovi2', 'thaovi2', 'thao@gmail.com2', '123456', 1, 3, NULL, '2016-08-10 14:03:03', '2016-08-10 14:05:26'),
(53, 'mangcau2', 'mangcau2', 'mangcau@gmail.com2', 'mangcau', 1, 3, NULL, '2016-08-10 14:09:30', '2016-08-10 14:09:30'),
(54, 'thanhlong2', 'thanhlong2', 'thanhlong@gmail.com2', 'thanhlong', 1, 3, NULL, '2016-08-10 14:09:46', '2016-08-10 14:09:46'),
(55, 'thuanvu12', 'thuanvu12', 'thuan1vu@gmail.com2', '123456', 1, 1, NULL, '2016-08-10 14:02:22', '2016-08-10 14:02:22'),
(56, 'quanvu12', 'quanvu12', 'quan@gmail.com12', '123456', 1, 1, NULL, '2016-08-10 14:02:39', '2016-08-10 14:05:39'),
(57, 'thaovi12', 'thaovi12', 'thao@gmail.com12', '123456', 1, 3, NULL, '2016-08-10 14:03:03', '2016-08-10 14:05:26'),
(58, 'mangcau12', 'mangcau12', 'mangcau@gmail.com12', 'mangcau', 1, 3, NULL, '2016-08-10 14:09:30', '2016-08-10 14:09:30'),
(59, 'thanhlong12', 'thanhlong12', 'thanhlong@gmail.com12', 'thanhlong', 2, 3, NULL, '2016-08-10 14:09:46', '2016-08-11 00:44:51'),
(60, 'aaaaaa', 'aaaaaaa', 'conheobo@gmail.com', '12345678', 1, 2, NULL, '2016-08-11 00:59:39', '2016-08-11 00:59:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_status`
--

CREATE TABLE `user_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_status`
--

INSERT INTO `user_status` (`id`, `status_name`, `created_at`, `updated_at`) VALUES
(1, 'Active', '2016-08-10 10:00:00', '2016-08-10 10:00:00'),
(2, 'Inactive', '2016-08-10 10:00:00', '2016-08-10 10:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_index` (`product_id`),
  ADD KEY `carts_user_id_index` (`user_id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorites_product_id_index` (`product_id`),
  ADD KEY `favorites_user_id_index` (`user_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_index` (`user_id`),
  ADD KEY `orders_stat_id_index` (`stat_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_index` (`order_id`),
  ADD KEY `order_details_product_id_index` (`product_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_product_categ_id_index` (`product_categ_id`),
  ADD KEY `products_user_id_index` (`user_id`);

--
-- Indexes for table `product_categs`
--
ALTER TABLE `product_categs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stats`
--
ALTER TABLE `stats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_status_id_index` (`status_id`),
  ADD KEY `users_role_id_index` (`role_id`);

--
-- Indexes for table `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `product_categs`
--
ALTER TABLE `product_categs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stats`
--
ALTER TABLE `stats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `user_status`
--
ALTER TABLE `user_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_stat_id_foreign` FOREIGN KEY (`stat_id`) REFERENCES `stats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_product_categ_id_foreign` FOREIGN KEY (`product_categ_id`) REFERENCES `product_categs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `user_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
