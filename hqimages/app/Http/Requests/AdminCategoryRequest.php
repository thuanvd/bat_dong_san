<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminCategoryRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'name' => 'required|min:2|max:50',
		];
	}
	public function messages() {
		return [
			'name.required' => 'Please insert name!',
			'name.min' => 'Name at least 2 char!',
		];
	}
}
