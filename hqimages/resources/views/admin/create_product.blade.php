@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Create Product!
        </p>
    @endif
         @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="simple-text"><h3>Create Products</h3></div>
            {!! Form::open(array('method'=>'POST', 'route'=>'admin.products.store',  'files' => true)) !!}
            <div class="form-group">
                {!! Form::label('Name')!!}
                {!! Form::text('product_name',null,array('required','class'=>'form-control','placeholder'=>'Product name...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Description')!!}
                {!! Form::textarea('product_des',null,array('required','class'=>'form-control','placeholder'=>'Product desciptions...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Category')!!}

                    {!!  Form::select('product_categ_id', $categs,null, ['required','class'=>'form-control','placeholder' => 'Pick a Category...']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Unit Price')!!}
                {!! Form::number('unit_price',0,array('required','class'=>'form-control','placeholder'=>'Product unit price...')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Downloads')!!}
                {!! Form::number('download',0,array('required','class'=>'form-control','placeholder'=>'Product download...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Views')!!}
                {!! Form::number('views',0,array('required','class'=>'form-control','placeholder'=>'Product views...')) !!}
            </div>
             <div class="form-group">
                {!! Form::label('User upload: ')!!}
                @foreach ($admin_infos as $admin_info)
                <i><b> {{ $admin_info->account }} </b></i>
                @endforeach

            </div>


                <label for="" class="control-label">Select Image</label>
                <div>
                    <div id="myfileupload">
                        <input class="btn btn-success" type="file" id="uploadfile"  name="uploadfile" onchange="readURL(this);"/>
                    </div>
                    <div id="thumbbox">
                        <img class="img" height="50%" width="50%" alt="Thumb image" id="thumbimage" style="display: none" />
                        <a class="removeimg" href="javascript:" ></a>
                    {{--</div>--}}
                    {{--<div id="boxchoice">--}}
                        {{--<a href="javascript:" class="Choicefile">Chọn avatar</a>--}}
                        {{--<p style="clear:both"></p>--}}
                    {{--</div>--}}
                    <label class="filename"></label>
                </div> <br>

            <div class="form-group">

                {{ Form::submit('Add New Products', array('class' => 'btn btn-primary')) }}

            </div>
            {!! Form::close() !!}
            <a href="{{url('admin/users')}}"> <button class="btn btn-info">Back</button></a>
        </div>
    </div>


    <script type="text/javascript">
        function readURL(input,thumbimage) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#thumbimage").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $("#thumbimage").show();
            }
            else {
                $("#thumbimage").attr('src', input.value);
                $("#thumbimage").show();
            }
            $('.filename').text($("#uploadfile").val());
            $('.Choicefile').css('background', '#C4C4C4');
            $('.Choicefile').css('cursor', 'default');
            $(".Choicefile").unbind('click');
            $(".removeimg").show();
        }
        $(document).ready(function () {
            $(".Choicefile").bind('click', function () {
                $("#uploadfile").click();

            });
            $(".removeimg").click(function () {
                $("#thumbimage").attr('src', '').hide();
                $("#myfileupload").html('<input type="file" id="uploadfile" onchange="readURL(this);" />');
                $(".removeimg").hide();
                $(".Choicefile").bind('click', function () {
                    $("#uploadfile").click();
                });
                $('.Choicefile').css('background','#0877D8');
                $('.Choicefile').css('cursor', 'pointer');
                $(".filename").text("");
            });
        })
    </script>
@endsection