@extends('layouts.layouts')
@section('breadcrumb')
@section('breadcrumb')
  <li><a href="{{Asset('/')}}">Home</a></li>
    <li class="active">Sign in</li>
@endsection
@section('content')
<div class="row">
<div class="col-md-4"></div>
  <div class="col-md-4">


<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Sign in</h3>
  </div>
  <div class="panel-body">
  @if (session('signup_success'))
    <div class="alert alert-success">
        {{ session('signup_success') }}
    </div>
@endif
  @if (session('signin_fail'))
    <div class="alert alert-danger">
        {{ session('signin_fail') }}
    </div>
@endif
  @if (session('signin_status'))
    <div class="alert alert-danger">
        {{ session('signin_status') }}
    </div>
@endif
     @if (count($errors) > 0)
					<div class="alert alert-danger">
					    <ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
					@endif
				 <form method="post" action="{{Asset('user/signin')}}" id="form_login">
                        <div class="form-group">
                            <label for="username" class="control-label">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Type your Username...">
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Type your Password...">
                        </div>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-success">Login</button>
                </div>
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                </form>

			<p style="text-align:left;"><a href="{{ URL::previous()}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a></p>

		 	</div>
  </div>
</div>


<div class="col-md-4">

</div>



@endsection