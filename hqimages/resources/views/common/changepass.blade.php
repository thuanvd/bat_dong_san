@extends('layouts.layouts')
@section('breadcrumb')

@endsection
@section('content')
<div class="row">
<div class="col-md-4"></div>
  <div class="col-md-4">


<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Change Password</h3>
  </div>
  <div class="panel-body">
  @include('flash::message')
            <script>
                $('#flash-overlay-modal').modal();
            </script>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
				<form method="post" action="{{Asset('changepass')}}/{{Session::get('id')}}" id="form_signup">

                        <div class="form-group">
                            <label for="password" class="control-label">New Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Type your new Password..">
                        </div>
                        <div class="form-group">
                            <label for="re_password" class="control-label">Re-New Password</label>
                            <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Type your new Password ...">
                        </div>


                </div>
                <div class="modal-footer" align="center">
                    <button type="submit" class="btn btn-success">Change</button>
                </div>
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                </form>

			<p style="text-align:left;"><a href="{{ URL::previous()}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a></p>

		 	</div>
  </div>
</div>


<div class="col-md-4">

</div>



@endsection