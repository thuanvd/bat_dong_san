@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Create User!
        </p>
    @endif
 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="simple-text"><h3>Create User</h3></div>
    {!! Form::open(array('method'=>'POST', 'route'=>'admin.users.store')) !!}
            <div class="form-group">
    {!! Form::label('Name')!!}
    {!! Form::text('name',null,array('required','class'=>'form-control','placeholder'=>'Name...')) !!}
            </div>
            <div class="form-group">
            {!! Form::label('Account')!!}
    {!! Form::text('account',null,array('required','class'=>'form-control','placeholder'=>'Account...')) !!}
            </div>
                <div class="form-group">
    {!! Form::label('Password')!!}
    {!! Form::text('password',null,array('required','class'=>'form-control','placeholder'=>'Password...')) !!}
                </div>
                    <div class="form-group">
    {!! Form::label('Email')!!}
    {!! Form::text('email',null,array('required','class'=>'form-control','placeholder'=>'Email...')) !!}
                    </div>
                        <div class="form-group">
                            {!! Form::label('Status')!!}
        {!!  Form::select('status_id', $status, null,['required','class'=>'form-control','placeholder' => 'Pick a Status...']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('Role')!!}
    {!!  Form::select('role_id',$roles , null, ['required','class'=>'form-control','placeholder' => 'Pick a Role...']) !!}
                        </div>
                            <div class="form-group">

            {{ Form::submit('Add New User', array('class' => 'btn btn-primary')) }}

                            </div>
    {!! Form::close() !!}
            <a href="{{url('admin/users')}}"> <button class="btn btn-info">Back</button></a>
        </div>
    </div>
@endsection