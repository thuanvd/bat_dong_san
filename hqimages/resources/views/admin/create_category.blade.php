@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Create Category!
        </p>
    @endif
     @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="simple-text"><h3>Create Category</h3></div>
            {!! Form::open(array('method'=>'POST', 'route'=>'admin.categories.store')) !!}
            <div class="form-group">
                {!! Form::label('Name')!!}
                {!! Form::text('name',null,array('required','class'=>'form-control','placeholder'=>'Name...')) !!}
            </div>
            <div class="form-group">

                {{ Form::submit('Add New Category', array('class' => 'btn btn-primary')) }}

            </div>
            {!! Form::close() !!}
            <a href="{{url('admin/categories')}}"> <button class="btn btn-info">Back</button></a>
        </div>
    </div>
@endsection