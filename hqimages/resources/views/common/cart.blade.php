@extends('layouts.layouts')
@section('breadcrumb')
        <li><a href="{{Asset('/')}}">Home</a></li>
        <li class=""><a href="{{Asset('update/')}}/{{Session::get('id')}}">User</a></li>
        <li class="active">Cart</li>
@endsection
@section('content')
    <div class="row" style="min-height: 300px;">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <div class="panel panel-info" >
                <!-- Default panel contents -->
                <div class="panel-heading" align="center"><h4>Your Cart</h4></div>
                @if (session('success'))
                    <div class="alert alert-success" align="center">
                        {{ session('success') }}
                    </div>
            @endif
            @if (session('no_order'))
    <div class="alert alert-warning" align="center">
        {{ session('no_order') }}
    </div>
@endif
                <!-- Table -->
                <table class="table table-striped">
                    <tr>
                        <td style="display:none;"><b>ID</b></td>
                         <td><b>#</b></td>
                        <td align="center"><b>Image</b></td>
                        <td><b>Name</b></td>
                        <td><b>Views</b></td>
                        <td><b>Download</b></td>
                        <td><b>Unit Price</b></td>

                        <td><b>Option</b></td>
                    </tr>
                    <?php $tong = 0;
$stt = 1;?>
                    @foreach($carts as $cart)
                        <?php $tong = $tong + $cart->unit_price;?>
                        <tr>
                            <td style="display:none;"><b>{{$cart->id}}</b></td>
                             <td><b>{{ $stt }}</b></td>
                            <td align="center"><img src="{{Asset('images')}}/{{$cart->path}}" style="width:80px; height: 80px;"></td>
                            <td>{{$cart->product_name}}</td>
                            <td>{{$cart->views}}</td>
                            <td>{{$cart->download}}</td>
                            <td id="{{$cart->id}}" class="price_unit">{{$cart->unit_price}}</td>

                            <td> <a href="{{Asset('cart/delete')}}/{{$cart->id}}"> <button type="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</button></a></td>
                        </tr>
                         <?php $stt = $stt + 1;?>
                    @endforeach
                    <tr>
                        <td style="display:none;"><b></b></td>
                        <td><b></b></td>
                        <td><b></b></td>
                        <td><b></b></td>
                        <td><b></b></td>
                        <td><b>Total</b></td>
                        <td><b>{{ number_format($tong) }} VNĐ</b></td>
                        <td><b></b></td>
                        <td><b></b></td>
                    </tr>
                </table>

            </div>
            <p style="text-align:left;">
                <a href="{{Asset('/')}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a>
                <a href="{{Asset('order')}}"><button id='order' class="btn btn-info " type="button"> <span class="glyphicon glyphicon-share"></span> Order <span class="glyphicon glyphicon-arrow-right"></span></button></a>

            </p>
        </div>
    </div>



    <div class="col-md-2">

    </div>



@endsection