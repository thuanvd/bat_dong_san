<?php
namespace App\Http\Controllers;
use App\Http\Requests\AdminProductRequest;
use App\Product;
use App\Product_categ;
use App\User;
use Illuminate\Http\Request;
use Image;

class AdminProductController extends Controller {
	public function index(Request $request) {
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		$products = Product::getproduct();
		return view('admin.products', ['admin_infos' => $admin_infos, 'products' => $products]);
	}
	public function destroy(Request $request, $id) {
		$user = Product::findOrFail($id);
		$user->delete();
		return redirect('admin/products')->with('message', 'Delete success!');
	}
	public function create(Request $request) {
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();

		// $userupload = User::findOrFail($admin_id);
		$categs = Product_categ::pluck('categ_name', 'id');
		return view('admin.create_product', ['admin_infos' => $admin_infos, 'categs' => $categs], compact($categs, $admin_infos))->with('message', 'Please type info!');
	}
	public function store(AdminProductRequest $request) {
		$admin_id = $request->session()->get('id');
		if ($request->hasFile('uploadfile')) {
			$name = $request->file('uploadfile')->getClientOriginalName();
			$nameimg = time() . '_' . $name;
			$request->file('uploadfile')->move('images', $nameimg);
			// $img = $request->file('uploadfile')->getRealPath();
			
			// Image::make($img)->resize('200', '200')->save(public_path() . '/thumbnail/' . $nameimg);

			// $img->resize(320, 240);
			// $img->save('$nameimg');

			$img=Image::make('images/' . $nameimg)->resize(250, 250)->save('thumbnail/' . $nameimg);

			// $img = Image::make($request->file('uploadfile')->getRealPath());
			// Image::make(public_path($request->file('uploadfile')))->resize(250, 250)->save('abc.jpg');
		} else {
			$nameimg = "default.png";
		}
		$data = $request->all();
		$product = new Product;
		$product->product_name = $data['product_name'];
		$product->product_des = $data['product_des'];
		$product->product_categ_id = $data['product_categ_id'];
		$product->unit_price = $data['unit_price'];
		$product->download = $data['download'];
		$product->views = $data['views'];
		$product->user_id = $admin_id;
		$product->path = $nameimg;
		$product->save();
		return redirect('admin/products')->with('message', 'Add new products success!');

	}
	public function edit(Request $request, $id) {
		$pro = Product::find($id);
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		$categs = Product_categ::pluck('categ_name', 'id');
		return view('admin.edit_product', ['admin_infos' => $admin_infos, 'pro' => $pro, 'categs' => $categs], compact($categs))->with('message', 'Please update!');
	}
	public function update(AdminProductRequest $request, $id) {
		if ($request->hasFile('uploadfile')) {
			$request->file('uploadfile')->move('images', 'ccccc');
		} else {
			echo 'adawdadawdawdadad';
		}
		$product = Product::find($id);
		$nametemp = $product->path;
		$data = $request->all();
		$product->product_name = $data['product_name'];
		$product->product_des = $data['product_des'];
		$product->product_categ_id = $data['product_categ_id'];
		$product->unit_price = $data['unit_price'];
		$product->download = $data['download'];
		$product->views = $data['views'];
		// if ($request->hasFile('uploadfile')) {

		// 	$name = $request->file('uploadfile')->getClientOriginalName();
		// 	var_dump($name);
		// 	// $nameimg = $name . '_' . $id;
		// 	$request->file('uploadfile')->move('images', $nametemp);
		// 	// $product->path = $nameimg;
		// }
		$product->save();
		return redirect('admin/products')->with('message', 'Update success!');
	}
}
