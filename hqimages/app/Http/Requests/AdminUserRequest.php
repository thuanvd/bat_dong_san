<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminUserRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'name' => 'required|min:2|max:30',
			'email' => 'required|email|unique:users,email,' . $this->id,
			'account' => 'required|min:6|max:20|unique:users,account,' . $this->id,
			'password' => 'required|min:6|max:20',
		];
	}
	public function messages() {
		return [
			'name.required' => 'Name can not null',
			'name.min' => 'Name must be at least 2 character',
			'email.email' => 'Email must email type',
			'account.required' => 'Account is not null',
			'account.min' => 'Account at least 6 character',
			'account.max' => 'Account too much 20 character',
			'password.required' => 'Email must email type',
			'password.min' => 'Password at least 6 character',
			'password.max' => 'Password too much 20 character',

			'email.required' => 'Email can not null',
			'email.unique' => 'Email have alrady register'];
	}
}
