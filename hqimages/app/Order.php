<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model {
	public static function getOrderID() {
		return DB::table('orders')->max('id');
	}
	public static function getUnConfirmOrder($user_id) {
		return DB::table('orders')->where('user_id', '=', $user_id)
			->join('users', 'users.id', '=', 'orders.user_id')
			->join('stats', 'stats.id', '=', 'orders.stat_id')
			->where('orders.user_id', '=', $user_id)
			->where('orders.stat_id', '=', 1)
			->select('orders.*', 'stats.stat_name', 'users.account')
			->orderBy('orders.id', 'desc')->get();

	}
	public static function getConfirmOrder($user_id) {
		return DB::table('orders')->where('user_id', '=', $user_id)
			->join('users', 'users.id', '=', 'orders.user_id')
			->join('stats', 'stats.id', '=', 'orders.stat_id')
			->where('orders.user_id', '=', $user_id)
			->where('orders.stat_id', '=', 2)
			->select('orders.*', 'stats.stat_name', 'users.account')
			->orderBy('orders.id', 'desc')->get();

	}
	public static function getInfoOrderdetail($order_id) {
		return DB::table('orders')->where('id', '=', $order_id)
			->get();
	}
	public static function getUnConOrder() {
		return DB::table('orders')
			->join('users', 'users.id', '=', 'orders.user_id')
			->join('stats', 'stats.id', '=', 'orders.stat_id')
			->where('orders.stat_id', '=', 1)
			->select('orders.*', 'stats.stat_name', 'users.account')
			->orderBy('orders.id', 'desc')->get();
	}
	public static function getConOrder() {
		return DB::table('orders')
			->join('users', 'users.id', '=', 'orders.user_id')
			->join('stats', 'stats.id', '=', 'orders.stat_id')
			->where('orders.stat_id', '=', 2)
			->select('orders.*', 'stats.stat_name', 'users.account')
			->orderBy('orders.id', 'desc')->get();
	}
	public static function getOrder($id) {
		return DB::table('orders')
			->join('users', 'users.id', '=', 'orders.user_id')
			->join('stats', 'stats.id', '=', 'orders.stat_id')
			->where('orders.id', '=', $id)
			->select('orders.*', 'stats.stat_name', 'users.account')
			->orderBy('orders.id', 'desc')->get();

	}
	public static function getOrderall() {
		return DB::table('orders')
			->join('users', 'users.id', '=', 'orders.user_id')
			->join('stats', 'stats.id', '=', 'orders.stat_id')
			->select('orders.*', 'stats.stat_name', 'users.account')
			->orderBy('orders.id', 'desc')->get();

	}
}
