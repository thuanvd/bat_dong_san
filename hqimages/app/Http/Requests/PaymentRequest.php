<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PaymentRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'name' => 'required|min:2|max:30',
			'address' => 'required|min:2|max:50',
			'phone' => 'required|min:8|max:12',
		];
	}
}
