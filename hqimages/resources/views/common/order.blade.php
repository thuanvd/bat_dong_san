@extends('layouts.layouts')
@section('breadcrumb')
  <li><a href="{{Asset('/')}}">Home</a></li>
    <li class="active">Order</li>
@endsection
@section('content')
<div class="row">
<div class="col-md-1"></div>
  <div class="col-md-10">


<div class="panel panel-info">
  {{-- <div class="panel-heading">
    <h3 class="panel-title">Order Product</h3>
  </div> --}}
  <div class="panel-body">
  @if (session('signup_success'))
    <div class="alert alert-success">
        {{ session('signup_success') }}
    </div>
@endif

  @if (session('signin_fail'))
    <div class="alert alert-danger">
        {{ session('signin_fail') }}
    </div>
@endif
  @if (session('signin_status'))
    <div class="alert alert-danger">
        {{ session('signin_status') }}
    </div>
@endif
     @if (count($errors) > 0)
					<div class="alert alert-danger">
					    <ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
					@endif

<div class="row">


<div class="col-md-8">
 <table class="table table-striped">
                    <tr>
                        <td style="display:none;"><b>ID</b></td>
                        <td><b>#</b></td>
                        <td align="center"><b>Image</b></td>
                        <td><b>Name</b></td>
                        <td><b>Views</b></td>
                        <td><b>Download</b></td>
                        <td><b>Unit Price</b></td>


                    </tr>
                    <?php $tong = 0;
$stt = 1;
?>
                    @foreach($carts as $cart)
                        <?php $tong = $tong + $cart->unit_price;?>
                        <tr>
                            <td style="display:none;"><b>{{$cart->id}}</b></td>
                            <td><b>{{ $stt }}</b></td>
                            <td align="center"><img src="{{Asset('images')}}/{{$cart->path}}" style="width:100px; height: 100px;"></td>
                            <td>{{$cart->product_name}}</td>
                            <td>{{$cart->views}}</td>
                            <td>{{$cart->download}}</td>
                            <td id="{{$cart->id}}" class="price_unit">{{$cart->unit_price}}</td>


                        </tr>
                        <?php $stt = $stt + 1;?>
                    @endforeach
                    <tr>
                        <td style="display:none;"><b></b></td>
                        <td><b></b></td>
                        <td><b></b></td>
                        <td><b></b></td>
                        <td><b></b></td>
                        <td><b>Total</b></td>
                        <td><b>{{ number_format($tong) }} VNĐ</b></td>

                    </tr>
                </table>
  </div>
<div class="col-md-4">
<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">Your info</h3>
  </div>
  <div class="panel-body">
 {!! Form::open(array('method'=>'POST', 'url'=>'payment')) !!}
            <div class="form-group">
                {!! Form::label('Name')!!}
                {!! Form::text('name',null,array( 'required','class'=>'form-control','placeholder'=>'Vũ Đức Thuận')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Address')!!}
                {!! Form::textarea('address',null,array('rows'=>'2','required','class'=>'form-control','placeholder'=>'53 - Ấp Phụng Quới A - TT Thạnh An - H vĩnh Thạnh')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Phone number')!!}
                {!! Form::number('phone',null,array('required','class'=>'form-control','placeholder'=>'01667703090')) !!}
            </div>
            <div class="form-group">

                {{ Form::submit('Payment', array('class' => 'btn btn-success')) }}

            </div>
            {!! Form::close() !!}

  </div>
</div>

  </div>
</div>


			<p style="text-align:left;">
      <a href="{{ URL::previous()}}">
      <button class="btn btn-primary " type="button">
      <span class="glyphicon glyphicon-arrow-left"></span>
      Back
      </button>
      </a>
       <a href="{{Asset('/')}}">
      <button class="btn btn-info " type="button">
      <span class=""></span>
      Buy continue
      </button>
      </a>
      </p>

		 	</div>
  </div>
</div>


<div class="col-md-1">

</div>

<script>
  $(document).ready(function(){
    $("#post_payment").submit(function(e){
        if(confirm('Are you sure?')){

            return true;
        }
        e.preventDefault();
        return false;
    });
});
</script>


@endsection