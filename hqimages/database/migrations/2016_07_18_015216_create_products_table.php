<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('products', function (Blueprint $table) {
			$table->increments('id');
			$table->string('product_name');
			$table->string('product_des');
			$table->integer('product_categ_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->index();

			$table->string('path');
			$table->string('unit_price');
			$table->integer('download');
			$table->integer('views');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('product_categ_id')->references('id')->on('product_categs')->onDelete('cascade')->onUpdate('cascade');
			$table->engine = 'InnoDB';

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('products');
	}
}
