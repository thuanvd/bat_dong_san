<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Product_categ extends Model {
	public static function searchcateg($query) {
		return DB::table('product_categs')->where('categ_name', 'like', "%" . $query . "%")
			->take(10)->get();
	}
}
