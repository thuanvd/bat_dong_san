<?php

namespace App\Http\Controllers;
use App\Http\Requests\PaymentRequest;
// use App\Http\Requests\Request;
use App\Order;
use App\Order_detail;
use DB;
use Illuminate\Http\Request;

class PaymentController extends Controller {
	public function index(PaymentRequest $request) {
		$data = $request->all();
		$user_id = $request->session()->get('id', 'default');
		$cart_array = $request->session()->get('cart.id', []);
		$array_unique = array_unique($cart_array);
		$arr_pro = DB::table('products')
			->whereIn('id', $array_unique)->get();
		$sum_price = DB::table('products')
			->whereIn('id', $array_unique)->sum('unit_price');

		$order = new Order;
		$order->user_id = $user_id;
		$order->stat_id = 1;
		$order->name = $data['name'];
		$order->address = $data['address'];
		$order->phone = $data['phone'];
		$order->price = $sum_price;
		$order->save();

		$order_id = Order::getOrderID();
		foreach ($arr_pro as $key) {
			$order_detail = new Order_detail;
			$order_detail->order_id = $order_id;
			$order_detail->product_id = $key->id;
			$order_detail->unit_price = $key->unit_price;
			$order_detail->save();
		}
		$categs = DB::table('product_categs')->get();
		$request->session()->forget('cart.id');
		$request->session()->forget('key');

		return redirect('confirm');
//		$unconfirms = Order::getUnConfirmOrder($user_id);
		//		$confirms = Order::getConfirmOrder($user_id);
		//		return view('common.confirm', ['unconfirms' => $unconfirms, 'confirms' => $confirms])->with('categs', $categs);
	}
	public function getConfirm(Request $request) {
		$categs = DB::table('product_categs')->get();
		return view('common.confirm')->with('categs', $categs);
	}
	public function billindex(Request $request) {
		$categs = DB::table('product_categs')->get();
		$user_id = $request->session()->get('id', 'default');
		$unconfirms = Order::getUnConfirmOrder($user_id);
		$confirms = Order::getConfirmOrder($user_id);

		if (!$unconfirms) {
			$request->session()->flash('no_uncon', 'There no Order Unconfirm');
		}
		if (!$confirms) {
			$request->session()->flash('no_con', 'There no Order Confirm');
		}
		return view('common.bill', ['unconfirms' => $unconfirms, 'confirms' => $confirms])->with('categs', $categs);
	}
	public function billdetail(Request $request, $stat, $id) {
		$categs = DB::table('product_categs')->get();
		$user_id = $request->session()->get('id', 'default');
		$infoOrders = Order::getInfoOrderdetail($id);
		if ($stat == 1) {
			$confirms = Order::getUnConfirmOrder($user_id);
			$order_details = Order_detail::getUnConfirmBillDetail($id);
			return view('common.billdetail', ['confirms' => $confirms, 'order_details' => $order_details])->with('categs', $categs)->with('stat', $stat)
				->with('infoOrders', $infoOrders);
		} else {
			$confirms = Order::getConfirmOrder($user_id);
			$order_details = Order_detail::getConfirmBillDetail($id);
			return view('common.billdetail', ['confirms' => $confirms, 'order_details' => $order_details])->with('categs', $categs)->with('stat', $stat)
				->with('infoOrders', $infoOrders);
		}

	}
	public function updatepayment(PaymentRequest $request, $stat, $id) {
		$data = $request->all();
		$order = Order::find($id);
		$order->name = $data['name'];
		$order->address = $data['address'];
		$order->phone = $data['phone'];
		$order->save();
		return redirect('bill')->with('update_suc', 'Update success!');
	}
	public function download($id, $path) {
		DB::table('products')->where('id', $id)->increment('download', 1);
		return view('common.download')->with('path', $path);
	}
}
