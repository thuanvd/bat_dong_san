@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Order details!
        </p>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <table class="table table-striped">
            
                <thead>
                <tr>
                    <th>#</th>
                    <th style="display: none">ID</th>

                    <th>Name</th>
                    <th>Unit Price</th>
                    <th>Image</th>
                    <th>Option</th>

                </tr>
                </thead>
                <tbody>
                <?php $stt = 1;?>
                @foreach ($order_details as $order_detail)
                    {{-- expr --}}

                    <tr>
                        <td>{{ $stt }}</td>
                        <td style="display: none">{{ $order_detail->id }}</td>

                        <td>{{  $order_detail->product_name}}</td>
                        <td>{{  $order_detail->unit_price }}</td>
                        <td><img src="{{ Asset('images') }}/{{  $order_detail->path }}" style="width: 100px; height: 100px;" alt=""></td>
                        <td >
                            {!!  Form::open(['method' => 'DELETE','id'=>'delete_pro','route' => ['admin.orders.details.destroy',$order_detail->id]])  !!}
                            {!!   Form::submit('Delete',['class' => 'btn btn-danger', 'id'=>'delete']) !!}
                            {!!  Form::close() !!}

                        </td>





                    </tr>
                    <?php $stt = $stt + 1;?>
                @endforeach
                </tbody>

            </table>
            <a href="{{ URL::previous()}}">
      <button class="btn btn-primary " type="button">
      <span class=""></span>
      Back
      </button>
      </a>
        </div>
        <div class="col-md-3"></div>
    </div>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

    <script language="JavaScript" type="text/javascript">
        $(document).ready(function(){
            $("input#delete").click(function(e){
                if(!confirm('Are you sure?')){
                    e.preventDefault();
                    return false;
                }
                return true;
            });
        });
    </script>
@endsection