<meta charset=utf-8>
<meta name=description content="">
<meta name=viewport content="width=device-width, initial-scale=1">
<!--Autocomplete-->
<link rel="stylesheet" href="{{Asset('style/jquery-ui-1.11.2/jquery-ui.css')}}">
<script src="{{Asset('style/jquery-ui-1.10.4/js/jquery-1.10.2.js')}}"></script>
<script src="{{Asset('style/jquery-ui-1.11.2/jquery-ui.js')}}"></script>
<!--Kết thúc Autocomplete-->
<script>
  $.widget( "custom.catcomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        var li;
        if ( item.category != currentCategory ) {
          ul.append( "<li style='margin-top:10px;' class='ui-autocomplete-category'>" + item.category + "</li>" );
          currentCategory = item.category;
        }
        li = that._renderItemData( ul, item );
        if ( item.category ) {
          li.html("<a  href='"+item.val+"'>" +item.hinh+" "+item.label+"</a>");
        }
      });
    }
  });

  data = [@if (isset($data) && isset($data2))
      @foreach ($data as $key)
        {label:"{{$key->product_name}}",category: "Product",val: "{{Asset('product')}}/{{$key->id}}", hinh: "<img src='{{Asset('images')}}/{{$key->path}}' width='100' hight='250'>"},
      @endforeach
            @foreach ($data2 as $key)
        {label:"{{$key->categ_name}}",category: "Categories",val: "{{Asset('categ')}}/{{$key->id}}", hinh: ''},
      @endforeach

    @endif ];

  $( "#searchInput" ).catcomplete({
      delay:-1,
      source: data
    });

</script>
