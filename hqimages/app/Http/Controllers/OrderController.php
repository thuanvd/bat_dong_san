<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class OrderController extends Controller {
	public function index(Request $request) {
		if ($request->session()->has('id')) {
			$array_cart = $request->session()->get('cart.id', []);
			if(empty($array_cart)) {
				return back()->with('no_order', 'Nothing in your cart...!');
			}
			$categs = DB::table('product_categs')->get();
			// $array_cart = $request->session()->get('cart.id', []);
			$array_unique = array_unique($array_cart);
			$cart = DB::table('products')
				->whereIn('id', $array_unique)->get();
			return view('common.order')->with('categs', $categs)->with('carts', $cart);
		}
		// return redirect('user/signin')->with('signin_fail', 'Please Signin!');
	}
}
