@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Update Order!
        </p>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="simple-text"><h3>Update Order {{$orders->id}}</h3></div>
              <table class="table">
                <thead>

                <tr>
                    <th>#</th>
                    <th style="display: none">ID</th>
                    <th>Name</th>
                    <th>Unit Price</th>
                    <th>Image</th>
                    <th>Option</th>

                </tr>

                </thead>
                <tbody>
                 @if (session('no_pro'))
                <div class="alert alert-danger" align="center">
                    {{ session('no_pro') }}
                </div>
                @endif
                <?php $stt = 1;?>
                @foreach ($order_details as $order_detail)
                    {{-- expr --}}

                    <tr>
                        <td>{{ $stt }}</td>
                        <td style="display: none">{{ $order_detail->id }}</td>

                        <td>{{  $order_detail->product_name}}</td>
                        <td>{{  $order_detail->unit_price }}</td>
                        <td><img src="{{ Asset('images') }}/{{  $order_detail->path }}" style="width: 100px; height: 100px;" alt=""></td>
                        <td >
                            {!!  Form::open(['method' => 'DELETE','id'=>'delete_pro','route' => ['admin.orders.details.destroy',$order_detail->id]])  !!}
                            {!!   Form::submit('Delete',['class' => 'btn btn-danger', 'id'=>'delete']) !!}
                            {!!  Form::close() !!}

                        </td>





                    </tr>
                    <?php $stt = $stt + 1;?>
                @endforeach
                </tbody>

            </table>
            <hr>
            {!! Form::model($orders, array('method'=>'PATCH', 'route'=>array('admin.orders.update', $orders->id))) !!}
            
         
            <div class="form-group">
                {!! Form::label('Name')!!}
                {!! Form::text('name',$orders->name,array('required','class'=>'form-control','placeholder'=>'Password...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Address')!!}
                {!! Form::text('address',$orders->address,array('required','class'=>'form-control','placeholder'=>'Address...')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('Phone')!!}
                {!! Form::text('phone',$orders->phone,array('required','class'=>'form-control','placeholder'=>'Phone...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Price')!!}
                {!! Form::text('price',$orders->price,array('required','class'=>'form-control','placeholder'=>'Price...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Status')!!}
                {!!  Form::select('stat_id', $status, $orders->stat_id, ['required','class'=>'form-control','placeholder' => 'Pick a Status...']) !!}
               
            </div>
            <div class="form-group">
                
                {{ Form::submit('Update Order', array('class' => 'btn btn-primary')) }}
            
            </div>
            {!! Form::close() !!}
            <a href="{{url('admin/orders')}}"> <button class="btn btn-info">Back</button></a>
        </div>
    </div>
     <div class="col-md-2"></div>
@endsection