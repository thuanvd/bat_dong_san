<?php

namespace App\Http\Controllers;
use App\Category;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminController extends Controller
{
    public function index(Request $request){
        $count_users = User::all()->count();
        $count_categ = Category::all()->count();
        $count_product = Product::all()->count();
        $count_orders = Order::all()->count();
        $admin_id = $request->session()->get('id');
        $admin_infos = User::where('id',$admin_id)->get();
        return view('admin.admin',['admin_infos' => $admin_infos,
                                    'categ'=>$count_categ,
                                    'product'=>$count_product,
                                    'users'=>$count_users,
                                    'orders'=>$count_orders]);
    }
}
