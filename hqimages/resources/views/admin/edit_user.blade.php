@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Update User!
        </p>
    @endif
 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="simple-text"><h3>Update User {{$user->name}}</h3></div>
    {!! Form::model($user, array('method'=>'PATCH', 'route'=>array('admin.users.update', $user->id))) !!}
     {!! Form::hidden('id', $user->id) !!}
            <div class="form-group">
    {!! Form::label('name','Name')!!}
    {!! Form::text('name',$user->name,array('required','class'=>'form-control','placeholder'=>'Name...')) !!}
            </div>
            <div class="form-group">
            {!! Form::label('Account')!!}
    {!! Form::text('account',$user->account,array('required','class'=>'form-control','placeholder'=>'Account...')) !!}
            </div>
                <div class="form-group">
    {!! Form::label('Password')!!}
    {!! Form::text('password',$user->password,array('required','class'=>'form-control','placeholder'=>'Password...')) !!}
                </div>
                    <div class="form-group">
    {!! Form::label('Email')!!}
    {!! Form::text('email',$user->email,array('required','class'=>'form-control','placeholder'=>'Email...')) !!}
                    </div>
                        <div class="form-group">
    {!! Form::label('Status')!!}
        {!!  Form::select('status_id', $status, $user->status_id, ['required','class'=>'form-control','placeholder' => 'Pick a Status...']) !!}
        {!! Form::label('Role')!!}
    {!!  Form::select('role_id', $roles, $user->role_id, ['required','class'=>'form-control','placeholder' => 'Pick a Role...']) !!}
                        </div>
                            <div class="form-group">

            {{ Form::submit('Update User', array('class' => 'btn btn-primary')) }}

                            </div>
    {!! Form::close() !!}
            <a href="{{url('admin/users')}}"> <button class="btn btn-info">Back</button></a>
        </div>
    </div>
@endsection