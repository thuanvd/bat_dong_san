<?php

namespace App\Http\Controllers;
use App\Product;
use DB;

class ProductController extends Controller {
	public function getIndex($id_pro) {
		$pro_details = Product::where('id', $id_pro)->get();
		$categs = DB::table('product_categs')->get();
		$views = DB::table('products')->where('id', $id_pro)->increment('views', 1);
		return view('common.product_detail', ['pro_details' => $pro_details])->with('categs', $categs);
	}

}
