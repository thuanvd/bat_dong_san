$('#form_login').validate({
    rules:{
    username: {
      required: true,
      minlength: 4
    },
    password: {
      required: true,
      minlength: 6
    },

  },
  messages: {
    username: {
      required: "Bạn vui lòng nhập Tài khoản",
      minlength: "Bạn vui lòng nhập ít nhất 4 ký tự"
    },
    password: {
      required: "Bạn vui lòng nhập Password",
      minlength: "Bạn vui lòng nhập ít nhất 6 ký tự"
    },
  },
   success: function() {
    $(this).addClass("valid")
    }

});

