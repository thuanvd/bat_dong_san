<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
	protected $table = 'products';
	public static function getproduct() {
		return DB::table('products')
			->join('product_categs', 'product_categs.id', '=', 'products.product_categ_id')
			->join('users', 'users.id', '=', 'products.user_id')
			->select('products.*', 'product_categs.categ_name', 'users.account')
			->orderBy('products.id', 'desc')
			->get();
	}
	public static function searchimage($query) {
		return DB::table('products')->where('product_name', 'like', "%" . $query . "%")
			->take(10)->get();
	}
}
