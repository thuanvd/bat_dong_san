@extends('layouts.layouts')
@section('breadcrumb')
    <li><a href="{{Asset('/')}}">Home</a></li>
    <li class=""><a href="{{Asset('update/')}}/{{Session::get('id')}}">User</a></li>
    <li class="active">Favorites</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8" style="min-height: 300px;">
    
            <div class="panel panel-info">
                <!-- Default panel contents -->
                <div class="panel-heading" align="center"><h4>Your Favorite Product</h4></div>
                @if (session('success'))
                    <div class="alert alert-success" align="center">
                        {{ session('success') }}
                    </div>
            @endif
             @if (session('no_fav'))
    <div class="alert alert-warning" align="center">
        {{ session('no_fav') }}
    </div>
@endif
                <!-- Table -->
                <table class="table table-striped">
                    <tr>
                        <td align="center"><b>Image</b></td>
                        <td><b>Name</b></td>
                        <td><b>Price</b></td>
                        <td><b>Views</b></td>
                        <td><b>Download</b></td>
                        <td><b>Option</b></td>
                    </tr>
                    @foreach($favs as $fav)
                        <tr>
                            <td align="center"><img src="{{Asset('images')}}/{{$fav->path}}" style="width:80px; height: 80px;"></td>
                            <td>{{$fav->product_name}}</td>
                            <td>{{$fav->unit_price}}</td>
                            <td>{{$fav->views}}</td>
                            <td>{{$fav->download}}</td>
                            <td> <a href="{{Asset('favorite/delete')}}/{{$fav->id}}"> <button type="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</button></a>
                               <a href="#" class="btn btn-info" role="button" id="{{ $fav->id }}" name="addcart">
                                        <span class="glyphicon glyphicon-shopping-cart"></span> Cart</a>
                            </td>
                        </tr>
                    @endforeach
                    
                </table>
            </div>
            <p style="text-align:left;"><a href="{{Asset('/')}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a></p>


        </div>
        </div>
    </div>
    
    
    <div class="col-md-2">
    
    </div>

<script>
    
</script>

@endsection