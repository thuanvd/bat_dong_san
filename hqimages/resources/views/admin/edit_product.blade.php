@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Update Product!
        </p>
    @endif
     @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="simple-text"><h3>Update Products {{$pro->product_name}}</h3></div>
            {!! Form::model($pro, array('method'=>'PUT', 'route'=>array('admin.products.update',$pro->id,  'files' => true,'enctype' =>"multipart/form-data"))) !!}

{{-- {!! Form::open(array('method'=>'POST', 'route'=>array('admin.products.update',$pro->id,  'files' => true))) !!} --}}
   {{-- <input type="hidden" name="_method" value="PUT"> --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
{{-- <input type="hidden" name="_method" value="put"> --}}
            <div class="form-group">
                {!! Form::label('Name')!!}
                {!! Form::text('product_name',$pro->product_name,array('required','class'=>'form-control','placeholder'=>'Product name...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Description')!!}
                {!! Form::textarea('product_des',$pro->product_des,array('required','class'=>'form-control','placeholder'=>'Product desciptions...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Category')!!}
                {!!  Form::select('product_categ_id', $categs, $pro->product_categ_id, ['required','class'=>'form-control','placeholder' => 'Pick a Category...']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Unit Price')!!}
                {!! Form::number('unit_price',$pro->unit_price,array('required','class'=>'form-control','placeholder'=>'Product unit price...','maxlength' => 2)) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Downloads')!!}
                {!! Form::number('download',$pro->download,array('required','class'=>'form-control','placeholder'=>'Product download...')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Views')!!}
                {!! Form::number('views',$pro->views,array('required','class'=>'form-control','placeholder'=>'Product views...')) !!}
            </div>
            <img src="{{ Asset('images') }}/{{ $pro->path }}" style="width: 50%; height: 50%;">
            <br>

<div>
                {{--  <label for="" class="control-label">Select Image</label>

                    <div id="myfileupload">
                        <input class="btn btn-success" type="file" id="uploadfile"  name="uploadfile" onchange="readURL(this);"/>
                         {!!  Form::file('uploadfile') !!}
                    </div>
                    <div id="thumbbox">
                        <img class="img" height="50%" width="50%" alt="Thumb image" id="thumbimage" style="display: none" />
                        <a class="removeimg" href="javascript:" ></a>

                    <label class="filename"></label>
                </div>--}}
<br>
                <div class="form-group">

                    {{ Form::submit('Update Products', array('class' => 'btn btn-primary')) }}

                </div>
                {!! Form::close() !!}
                <a href="{{url('admin/products')}}"> <button class="btn btn-info">Back</button></a>
            </div>
        </div>
</div>
    <script type="text/javascript">
        function readURL(input,thumbimage) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#thumbimage").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $("#thumbimage").show();
            }
            else {
                $("#thumbimage").attr('src', input.value);
                $("#thumbimage").show();
            }
            $('.filename').text($("#uploadfile").val());
            $('.Choicefile').css('background', '#C4C4C4');
            $('.Choicefile').css('cursor', 'default');
            $(".Choicefile").unbind('click');
            $(".removeimg").show();
        }
        $(document).ready(function () {
            $(".Choicefile").bind('click', function () {
                $("#uploadfile").click();

            });
            $(".removeimg").click(function () {
                $("#thumbimage").attr('src', '').hide();
                $("#myfileupload").html('<input type="file" id="uploadfile" onchange="readURL(this);" />');
                $(".removeimg").hide();
                $(".Choicefile").bind('click', function () {
                    $("#uploadfile").click();
                });
                $('.Choicefile').css('background','#0877D8');
                $('.Choicefile').css('cursor', 'pointer');
                $(".filename").text("");
            });
        })
    </script>
@endsection