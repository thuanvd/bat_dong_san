<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminProductRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'product_name' => 'required|min:2|max:50',
			'product_des' => 'required|min:2|max:50',
			'product_categ_id' => 'required',
			'unit_price' => 'required',
			'download' => 'required',
			'views' => 'required',
		];
	}
	public function messages() {
		return [
			'product_name.required' => 'Please insert name!',
			'product_name.min' => 'Name at least 2 char!',
			'product_des.required' => 'Name at least 2 char!',
			'product_des.min' => 'Name at least 2 char!',

			'product_categ_id.required' => 'Category cannot null!',
			'unit_price.required' => 'Unit price cannot null!',
			'download.required' => 'Download cannot null!',
			'views.required' => 'Views cannot null!',

		];
	}
}
