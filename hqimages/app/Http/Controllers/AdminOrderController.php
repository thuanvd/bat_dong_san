<?php

namespace App\Http\Controllers;
use App\Order;
use App\Order_detail;
use App\Stat;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;

class AdminOrderController extends Controller {
	public function index(Request $request) {
		$users = User::getuserinfo();
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		$unorders = Order::getUnConOrder();
		$orders = Order::getConOrder();
		$orderalls = Order::getOrderall();
		return view('admin.orders', ['admin_infos' => $admin_infos, 'users' => $users])->with('unorders', $unorders)->with('orders', $orders)->with('orderalls', $orderalls);
	}
	public function destroy(Request $request, $id) {
		$orders = Order::findOrFail($id);
		$orders->delete();
		return redirect('admin/orders')->with('message', 'Delete success!');
	}
	public function edit(Request $request, $id) {
		$orders = Order::find($id);
		$order_details = Order_detail::getConBillDetail($id);
//        $orders = Order::getOrder($id);

		$status = Stat::pluck('stat_name', 'id');
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		if(!$order_details){
			$request->session()->flash('no_pro', 'There nothing!');
		}
		return view('admin.edit_order', ['admin_infos' => $admin_infos, 'orders' => $orders, 'status' => $status, 'order_details' => $order_details], compact($status))->with('message', 'Please update!');
	}
	public function update(PaymentRequest $request, $id) {
		$orders = Order::find($id);
		$data = $request->all();
		$orders->name = $data['name'];
		$orders->address = $data['address'];
		$orders->phone = $data['phone'];
		$orders->price = $data['price'];
		$orders->stat_id = $data['stat_id'];
		$orders->save();
		return redirect('admin/orders')->with('message', 'Update success!');

	}
	public function orderdetail(Request $request, $id) {
		$infoOrders = Order::getInfoOrderdetail($id);
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();

		$order_details = Order_detail::getConBillDetail($id);
		return view('admin.order_details', ['admin_infos' => $admin_infos, 'infoOrders' => $infoOrders, 'order_details' => $order_details]);
	}
	public function orderdetaildestroy(Request $request, $id) {
		$detail = Order_detail::find($id);
		$detail->delete();
		return redirect('admin/orders')->with('mess', 'Delete success');
	}
}
