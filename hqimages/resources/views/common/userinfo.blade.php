@extends('layouts.layouts')
@section('breadcrumb')

@endsection
@section('content')
<div class="row">
<div class="col-md-4"></div>
  <div class="col-md-4">


<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">User Infomation</h3>
  </div>
  <div class="panel-body">
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif


 @foreach($userinfos as $user)
 <form action="{{Asset('update')}}/{{$user->id}}" method="post" accept-charset="utf-8">
  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Type your Name..." value="{{$user->name}}">
                        </div>
                        <div class="form-group">
                            <label for="username" class="control-label">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Type your Username..." value="{{$user->account}}" disabled="true">
                        </div>
                            <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Type your Email..." value="{{$user->email}}">
                        </div>
                </div>
                <div class="modal-footer" align="center">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>

                 </form>
@endforeach


			<p style="text-align:left;"><a href="{{ URL::previous()}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a></p>

		 	</div>
  </div>
</div>


<div class="col-md-4">

</div>



@endsection