@extends('layouts.adminlayouts')
@section('content')
<div class="simple-text"><h3>Statistic</h3></div>
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Welcome to Admin Page!
        </p>
    @endif
<table class="table">
        <tr>
            <td><b>#</b></td>
            <td><b>NAME</b></td>
            <td><b>No.</b></td>
        </tr>
        <tr>
            <td>1</td>
            <td>Users</td>
            <td>{!! $users !!}</td>

        </tr>
        <tr>
            <td>2</td>
            <td>Categorys</td>
            <td>{!! $categ !!}</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Products</td>
            <td>{!! $product !!}</td>
        </tr>
    <tr>
        <td>4</td>
        <td>Orders</td>
        <td>{!! $orders !!}</td>
    </tr>
</table>
<textarea class="ckeditor" name="editor"></textarea>
      
@endsection
