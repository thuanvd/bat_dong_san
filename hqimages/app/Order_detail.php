<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model {
	protected $table = 'order_details';
	public static function getUnConfirmBillDetail($id) {
		return DB::table('order_details')
			->join('products', 'products.id', '=', 'order_details.product_id')
			->join('orders', 'orders.id', '=', 'order_details.order_id')
			->select('order_details.*', 'products.path', 'products.product_name', 'products.unit_price', 'orders.stat_id')
			->where('orders.stat_id', '=', 1)
			->where('order_id', '=', $id)->get();
	}
	public static function getConfirmBillDetail($id) {
		return DB::table('order_details')
			->join('products', 'products.id', '=', 'order_details.product_id')
			->join('orders', 'orders.id', '=', 'order_details.order_id')
			->select('order_details.*', 'products.product_name', 'products.path', 'products.unit_price', 'orders.stat_id')
			->where('orders.stat_id', '=', 2)
			->where('order_id', '=', $id)->get();
	}
    public static function getConBillDetail($id) {
        return DB::table('order_details')
            ->join('products', 'products.id', '=', 'order_details.product_id')
            ->join('orders', 'orders.id', '=', 'order_details.order_id')
            ->select('order_details.*', 'products.product_name', 'products.path', 'products.unit_price', 'orders.stat_id')
            ->where('order_id', '=', $id)->get();
    }
    public static function getProDetail($id) {
        return DB::table('order_details')
            ->where('id', '=', $id)
            ->get();
    }
}
