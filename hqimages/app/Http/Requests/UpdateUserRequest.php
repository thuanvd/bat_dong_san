<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateUserRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'name' => 'required',
			'email' => 'required|unique:users,email',
		];
	}
	public function messages() {
		return [
			'name.required' => 'Name can not null',
			'email.required' => 'Email can not null',
			'email.unique' => 'Someone have this email'];
	}
}
