<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Admin Page</title>
{{-- 
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" /> --}}

    <!-- Bootstrap core CSS     -->
    {{-- <link href="{{Asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" /> --}}

    <!-- Animation library for notifications   -->
    {{-- <link href="{{Asset('assets/css/animate.min.css')}}" rel="stylesheet"/> --}}

    <!--  Paper Dashboard core CSS    -->
    <link href="{{Asset('assets/css/paper-dashboard.css')}}" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    {{-- <link href="{{Asset('assets/css/demo.css')}}" rel="stylesheet" /> --}}

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{Asset('assets/css/themify-icons.css')}}" rel="stylesheet">

    
    <script type="text/javascript" src="{{Asset('ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{Asset('ckfinder/ckfinder.js')}}"></script>
    
    
    <script language="JavaScript" src="https://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">


</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

        <!--
            Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
            Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                        Menu
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="{{ Asset('admin') }}">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @if(Session::get('role_id')==1)
                <li>
                    <a href="{{Asset('admin/users')}}">
                        <i class="ti-user"></i>
                        <p>Users Manage</p>
                    </a>
                </li>
                 <li>
                    <a href="{{Asset('admin/orders')}}">
                        <i class="ti-pencil"></i>
                        <p>Orders</p>
                    </a>
                </li>
                @endif
{{--                 <li>
                    <a href="{{Asset('admin/sliders')}}">
                        <i class="ti-view-list-alt"></i>
                        <p>Sliders Manage</p>
                    </a>
                </li> --}}
                <li>
                    <a href="{{Asset('admin/categories')}}">
                        <i class="ti-text"></i>
                        <p>Categories</p>
                    </a>
                </li>
                <li>
                    <a href="{{Asset('admin/products')}}">
                        <i class="ti-pencil-alt2"></i>
                        <p>Products</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="{{ Asset('admin') }}">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-bell"></i>
                                <p>Administrator</p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($admin_infos as $admin_info)
                                <li><a href="#">Name: {{$admin_info->name}}</a></li>
                                <li><a href="#">Reg. Date: {{$admin_info->created_at}}</a></li>
                                @endforeach
                                    <li><a href="{{Asset('user/signout')}}">Logout</a></li>

                            </ul>
                        </li>

                    </ul>

                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    {{-- Content --}}
                    @yield('content')


                    {{-- Content --}}
                </div>
                <div class="row">
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <li>
                            <a href="#">
                                Create: Thuận Vũ
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Licenses
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="#">Thuận Vũ</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>
<script src="{{Asset('style/list.js')}}" type="text/javascript"></script>
<!--   Core JS Files   -->
{{-- <script src="{{Asset('assets/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
<script src="{{Asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="{{Asset('assets/js/bootstrap-checkbox-radio.js')}}"></script>

<!--  Charts Plugin -->
<script src="{{Asset('assets/js/chartist.min.js')}}"></script>

<!--  Notifications Plugin    -->
<script src="{{Asset('assets/js/bootstrap-notify.js')}}"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="{{Asset('assets/js/paper-dashboard.js')}}"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="{{Asset('assets/js/demo.js')}}"></script> --}}


<script>
    $(document).ready(function() {
    $('#datatable').dataTable();  
} );

</script>
<style type="text/css" media="screen">
    .pagination>li {
display: inline;
padding:0px !important;
margin:0px !important;
border:none !important;
}
.modal-backdrop {
  z-index: -1 !important;
}
/*
Fix to show in full screen demo
*/
iframe
{
    height:700px !important;
}

.btn {
display: inline-block;
padding: 6px 12px !important;
margin-bottom: 0;
font-size: 14px;
font-weight: 400;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
}

.btn-primary {
color: #fff !important;
background: #428bca !important;
border-color: #357ebd !important;
box-shadow:none !important;
}
.btn-danger {
color: #fff !important;
background: #d9534f !important;
border-color: #d9534f !important;
box-shadow:none !important;
}
</style>




<script type="text/javascript">
    $(document).ready(function(){

        demo.initChartist();

        $.notify({
            icon: 'ti-gift',
            message: $("p#mess").html()

        },{
            type: 'success',
            timer: 4000
        });

    });
</script>

</html>
