<?php

namespace App\Http\Controllers;
use App\Product;
use DB;

class CategoryController extends Controller {
	public function getIndex($id) {
		$products = Product::where('product_categ_id', $id)->paginate(9); // Lấy thông tin theo Eloquent
		$tops = Product::where('product_categ_id', $id)->orderBy('download', 'desc')->take(5)->get();
		$categs = DB::table('product_categs')->get(); // Lấy thông tin theo DB query
		// $views = DB::table('products')->where('id', $id_pro)->increment('views', 1);
		return view('common.category', ['products' => $products], ['tops' => $tops])->with('categs', $categs); // 2 cách lấy thông tin khác nhau giữ Eloquent và DB query
	}
}
