@extends('layouts.layouts')
@section('breadcrumb')

@endsection
@section('content')
<div class="row">
<div class="col-md-4"></div>
  <div class="col-md-4">


<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Sign Up</h3>
  </div>
  <div class="panel-body">
     @if (count($errors) > 0)
					<div class="alert alert-danger">
					    <ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
					@endif
				<form method="post" action="{{Asset('user/signup')}}" id="form_signup">
                    <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Type your Name...">
                        </div>
                        <div class="form-group">
                            <label for="username" class="control-label">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Type your Username...">
                        </div>
                            <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Type your Email...">
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Type your Password..">
                        </div>
                        <div class="form-group">
                            <label for="re_password" class="control-label">Re-Password</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Type your Password again...">
                        </div>


                </div>
                <div class="modal-footer" align="center">
                    <button type="submit" class="btn btn-success">Sign up</button>
                </div>
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                </form>

			<p style="text-align:left;"><a href="{{ URL::previous()}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a></p>

		 	</div>
  </div>
</div>


<div class="col-md-4">

</div>



@endsection