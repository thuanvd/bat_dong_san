@extends('layouts.adminlayouts')
@section('content')
    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Update Category!
        </p>
    @endif
     @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="simple-text"><h3>Update Category {{$categ->categ_name}}</h3></div>
            {!! Form::model($categ, array('method'=>'PATCH', 'route'=>array('admin.categories.update', $categ->id))) !!}
            <div class="form-group">
                {!! Form::label('name','Name')!!}
                {!! Form::text('name',$categ->categ_name,array('required','class'=>'form-control','placeholder'=>'Name...')) !!}
            </div>
            <div class="form-group">
                {{ Form::submit('Update Category', array('class' => 'btn btn-primary')) }}

            </div>
            {!! Form::close() !!}
            <a href="{{url('admin/categories')}}"> <button class="btn btn-info">Back</button></a>
        </div>
    </div>
@endsection