@extends('layouts.layouts')
@section('breadcrumb')
	<li><a href="{{Asset('/')}}">Home</a></li>
	@foreach($pro_details as $pro_detail)
		<li class="active">{{$pro_detail->product_name}}</li>
		@endforeach
		
@endsection
@section('content')
<div class="row">
	<div class="col-md-1"></div>
  <div class="col-md-7">
	  	<div class="panel panel-success">
			<div class="panel-heading">
			    <h3 class="panel-title" align="center">Product</h3>
			  </div>
			  <div class="panel-body" align="center">
			    @foreach($pro_details as $pro_detail)
			    	<img src="{{Asset('thumbnail')}}/{{$pro_detail->path}}" style="width:100%; height: 100%;">
			    @endforeach
			  </div>
			</div>


		 	</div>

<div class="col-md-3">
	  	<div class="panel panel-success">
			<div class="panel-heading">
			    <h3 class="panel-title">Product Infomation</h3>
			  </div>
			  <div class="panel-body">
			    @foreach($pro_details as $pro_detail)
			    <h3><b>{{$pro_detail->product_name}}</b></h3>
			    	<h4><span class="glyphicon glyphicon-tag"></span> <b>Description:</b> {{$pro_detail->product_des}}</h4>
			    	<h4><span class="glyphicon glyphicon-usd"></span> <b>Price:</b> {{$pro_detail->unit_price}}</h4>
			    	<h4><span class="glyphicon glyphicon-download-alt"></span> <b>Download:</b> {{$pro_detail->download}}</h4>
			    @endforeach
			    	 <p><a href="#" class="btn btn-success" role="button" id="{{ $pro_detail->id }}" name="addcart">
	        <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart </a>
	         <a href="#" class="btn btn-danger" role="button" id="{{ $pro_detail->id }}" name="favorite">
	         <span class="glyphicon glyphicon-heart"></span> Add to Favorites</a> <a href="{{ URL::previous()}}"><button class="btn btn-primary " type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a>
</p>
			  </div>
			</div>
		 	</div>
    <div class="col-md-1"></div>

</div>

@endsection