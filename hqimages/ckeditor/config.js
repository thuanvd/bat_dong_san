/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	 	config.uiColor = '#AADC6E';
    	config.extraPlugins = 'imageuploader';
        config.filebrowserBrowseUrl = '../../style/ckfinder/ckfinder.html';
        config.filebrowserImageBrowseUrl = '../../style/ckfinder/ckfinder.html?type=Images';
        config.filebrowserFlashBrowseUrl = '../../style/ckfinder/ckfinder.html?type=Flash';
        config.filebrowserUploadUrl = '../../style/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
        config.filebrowserImageUploadUrl = '../../style/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
        config.filebrowserFlashUploadUrl = '../../style/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};

