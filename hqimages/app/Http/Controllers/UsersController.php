<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Validator;

class UsersController extends Controller {
	public function getSignin(Request $request) {
		$categs = DB::table('product_categs')->get();
		return view("common.signin")->with('categs', $categs);
	}
	public function postSignin(Request $request) {
		$validator = Validator::make($request->all(), [
			'username' => 'required ',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('user/signup')
				->withErrors($validator)
				->withInput();
		}
		$username_input = $request->input('username');
		$password_input = $request->input('password');

		$count = DB::table('users')->where('account', $username_input)->where('password', $password_input)->count();
		$users = DB::table('users')->where('account', $username_input)->where('password', $password_input)->get();
		$status = DB::table('users')->where('account', $username_input)->where('password', $password_input)->value('status_id');
		if ($count > 0) {
			if ($status == 1) {
				foreach ($users as $user) {
					$request->session()->put('id', $user->id);
					$request->session()->put('acc', $user->account);
					$request->session()->put('name', $user->name);
					$request->session()->put('role_id', $user->role_id);
					$request->session()->put('user_id', $user->status_id);
					flash()->overlay('Login Success! Thanks for visit.', 'Login Success! Thanks for visit.');
					return redirect('/');
				}
			} else {
				return redirect('user/signin')->with('signin_status', 'Your account have been block or delete!');
			}

		} else {

			return redirect('user/signin')->with('signin_fail', 'Your username or password are not correct! Please try again!');
		}

	}

	public function postSignup(Request $request) {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'username' => 'required|unique:users,account',
			'email' => 'required|email|unique:users,email',
			'password' => 'required',
			'password_confirmation' => 'required|same:password',
		]);

		if ($validator->fails()) {
			return redirect('user/signup')
				->withErrors($validator)
				->withInput();
		} else {
			$name = $request->input('name');
			$username = $request->input('username');
			$password = $request->input('password');
			$re_password = $request->input('re_password');
			$email = $request->input('email');
			DB::table('users')->insert(
				['name' => $name,
					'account' => $username,
					'password' => $password,
					'email' => $email,
					'role_id' => 2,
					'status_id' => 1,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now()]
			);
			return redirect('user/signin')->with('signup_success', 'You are Sign Up done! Please Sign In with your Account!');
		}
		// return redirect('login');
	}
	public function getSignup(Request $request) {
		$categs = DB::table('product_categs')->get();
		return view("common.signup")->with('categs', $categs);
	}
	public function getSignout(Request $request) {
		$request->session()->flush();
		return redirect('/');
	}
}
