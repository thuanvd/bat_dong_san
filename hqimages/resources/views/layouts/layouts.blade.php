<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{Asset('images/icon/favicon.ico')}}">




    <title>Minh Nguyệt</title>
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Multi Level Push Menu -->
    <link rel="stylesheet" type="text/css" href="{{Asset('style/multimenu/css/component.css')}}">
    <link rel="stylesheet" type="text/css" href="{{Asset('style/multimenu/css/demo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{Asset('style/multimenu/css/icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{Asset('style/multimenu/css/normalize.css')}}">
    <!-- <link rel="stylesheet" type="text/css" href="{{Asset('style/multimenu/css/stylebonus.css')}}"> -->
    <script src="{{Asset('style/multimenu/js/modernizr.custom.js')}}"></script>
    <script src="{{Asset('style/multimenu/js/mlpushmenu.js')}}"></script>
    <script src="{{Asset('style/multimenu/js/classie.js')}}"></script>
    <!-- end Push -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- <link href="carousel.css" rel="stylesheet"> -->

    <link rel="stylesheet" type="text/css" href="{{Asset('style/style.css')}}">




    
       <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>



<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.js"></script>


</head>
<body>

    <!-- Push Wrapper -->
    <div class="mp-pusher" id="mp-pusher">
        <!-- mp-menu -->
        <nav id="mp-menu" class="mp-menu">
            <div class="mp-level">
                <h2 class="icon icon-world">DỰ ÁN</h2>
                <ul>
                    @foreach($categs as $categ)
                    <li><a class="" href="{{Asset('categ')}}/{{$categ->id}}">{{$categ->categ_name}}</a>
                    </li>
                    @endforeach
                </ul>

            </div>
        </nav>
        <!-- /mp-menu -->

        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{Asset('/')}}">Bất Động Sản</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{Asset('/')}}"><span class="glyphicon glyphicon-home"></span></a>
                        </li>
                        <li><a href="{{Asset('about-me')}}">Giới </a>
                        </li>
                        <li><a href="{{Asset('contact-me')}}">Liên hệ</a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                    @if(!Session::has('id'))
                        <li><a href="#" data-toggle="modal" data-target=".dangnhap">Đăng nhập</a>
                        </li>
                        <li><a href="{{Asset('user/signup')}}">Đăng ký</a>
                        </li>
                    @else

                    <div class="btn-group" style="margin-top: 8px">
                      <span class="btn btn-default"> <a style="color: hotpink;text-decoration: none;" href="{{Asset('update/')}}/{{Session::get('id')}}"><span class="glyphicon glyphicon-user"></span> {{Session::get('name')}}</a></span>
                      <span  class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                      </span>
                      <ul class="dropdown-menu">
                       @if(Session::get('role_id') ==1 || Session::get('role_id')== 2)
                        <li><a href="{{Asset('bill')}}"><span class="glyphicon glyphicon-shopping-cart"> </span> Đơn hàng của bạn</a></li>
                        <li><a href="{{Asset('changepass')}}"><span class="glyphicon glyphicon-wrench"> </span> Đổi password</a></li>
                        <li><a href="{{Asset('admin')}}"><span class="glyphicon glyphicon-cog"> </span> Admin</a></li>
                        <li role="separator" class="divider"></li>

                         <li><a href="{{Asset('user/signout')}}"><span class="glyphicon glyphicon-off"> </span> Thoát</a></li>
                        @else
                        <li><a href="{{Asset('bill')}}"><span class="glyphicon glyphicon-off"> </span> Đơn hàng</a></li>
                        <li><a href="{{Asset('changepass')}}"><span class="glyphicon glyphicon-off"> </span> Đổi password</a></li>
                        <li><a href="{{Asset('user/signout')}}"><span class="glyphicon glyphicon-off"> </span> Thoát</a></li>
                        @endif
                      </ul>
                    </div>

                    @endif

                        <li>
                            <div class="btn-group" role="group" style="margin-top:8px;">
                                <a href="{{Asset('carts')}}">
                                <button type="button" class="btn btn-info">
                                    <span class="glyphicon glyphicon-shopping-cart"></span> Giỏ hàng <span class="badge">
                                        <div id="cart">
                                        @if (Session::has('key'))
                                             {{ Session::get('key') }}
                                             @else
                                             0
                                        @endif

                                        </div>
                                        <div id="carttemp" style="display: none;">content</div>
                                    </span>
                                </button>
                                </a>
                                <a href="{{Asset('favorites')}}">
                                <button type="button" class="btn btn-danger ">
                                    <span class="glyphicon glyphicon-heart-empty"></span> Yêu thích <span class="badge">
                                       <div id="num">
                                              @if (Session::has('fav_count'))
                                             {{ Session::get('fav_count') }}
                                             @else
                                             0
                                        @endif
                                       </div>
                                    </span>
                                </button>
                                    </a>
                            </div>
                        </li>

                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input type="search" class="form-control" placeholder="quận 7, quận 1,v.v.." id="searchInput"  name="searchInput">
                            </div>
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Tìm kiếm</button>
                        </form>
                         <script>
                          $("#searchInput").keyup(function(e){
                                        $.ajaxSetup({
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                            }
                                        });
                          var query = $(this).val();
                          var url="{{Asset('search')}}/"+query;
                          $.ajax({
                            type: "POST",
                            data: "query",
                            url: url,
                            success: function(kq){
                              $('#searchInput').html(kq);
                            },
                            error: function(){
                            }
                          });
                        });
                      </script>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container-fluid">

            <div class="row" style="margin-top:60px;">
                <div class="col-md-1"></div>
                <div class="col-md-10">

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>

                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="{{Asset('image-slider')}}/abc.jpg" alt="Ảnh đẹp thiên nhiên" style="height: 700px; width: 100%;">
                                <div class="carousel-caption">
                                    <h2> Cảnh đẹp hùng vĩ - Ảnh chất lượng </h2>
                                </div>
                            </div>
                            <div class="item">
                                <img src="{{Asset('image-slider')}}/xyz.jpg" alt="Cảnh đẹp hùng vĩ" style="height: 700px; width: 100%;">
                                <div class="carousel-caption">
                                    <h2> Ảnh cực kỳ chất lượng</h2>
                                </div>
                            </div>
                            <div class="item">
                                <img src="{{Asset('image-slider')}}/ghj.png" alt="Cảnh đẹp hùng vĩ" style="height: 700px; width: 100%;">
                                <div class="carousel-caption">
                                    <h2> Ảnh cực kỳ chất lượng</h2>
                                </div>
                            </div>

                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                </div>
            </div>
            <div class="col-md-1"></div>

        </div>
        <div class="row">
            <div class="col-lg-10 col-md-offset-1">
                <ul class="nav navbar-nav" style="align:center">
                    <button class="btn btn-info" id="trigger"><h4>Tất Cả Dự Án </h4></button>
                </ul>
            </div>
            <div class="col-lg-10 col-md-offset-1">
                <ol class="breadcrumb">

                    @yield('breadcrumb')
                </ol>
            </div>
        </div>
        {{-- Content --}}
        @yield('content')
        {{-- Content --}}
        <br>
        <footer style="background:#CFCFCF" id="footer" >
         <div class="container">
          <div class="col-md-4">

                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                      Liên hệ <br>
                    Địa chỉ: xxxx <br>
                    Trụ sở :xxxx<br>
                    Mã số thuế:xxxx <br>
                    Email: xxxx<br>
                    Điện thoại: xxxx
                </div>
      </div>
          {{--   <hr>
            <div class="row" style="color: white">
                <div class="col-md-4">

                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                      Liên hệ <br>
                    Địa chỉ: Lầu 5, 122 Cao Thắng, Phường 4, Quận 3, TP HCM <br>
                    Trụ sở : 17 đường 6, KDC Khang An, phường Phú Hữu, quận 9, TP HCM <br>
                    Mã số thuế: 0312102053 <br>
                    Email: contact@ipartner.vn <br>
                    Điện thoại: (08) 6280 0659
                </div>
            </div> --}}
        </footer>

    </div>

    <!-- /container -->

    <!-- Large modal -->
    <div class="modal fade bs-example-modal-sm dangnhap" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel" align="center">Sign in</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{Asset('user/signin')}}" id="form_login">
                        <div class="form-group">
                            <label for="username" class="control-label">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Type your Username...">
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Type your Password...">
                        </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                </form>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function(){

         $("a[name='favorite']").on('click',function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

              var que= $(this).attr('id');

              var url="{{ Asset('favorite') }}/"+que;

               $.ajax({
                          type: "POST",
                          url: url,
                          data: "que",
                          async: true,
                          error: function(){

                          },
                          success: function(data){
                            $("#num").html(data);
                          }
                        })
                        return false;

                      });
    });

</script>
<script>
    $(document).ready(function(){

         $("a[name='addcart']").click(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

              var que= $(this).attr('id');

              var url="{{ Asset('addcart') }}/"+que;

               $.ajax({
                          type: "POST",
                          url: url,
                          data: "que",
                          async: true,
                          error: function(){

                          },
                          success: function(data){
                            $("#cart").html(data);
                           // console.log(status);
                          }
                        })
                        return false;
                      });
    });

</script>

 <script src="{{Asset('style/validation_login.js')}}"></script>
    <script>
        new mlPushMenu(document.getElementById('mp-menu'), document.getElementById('trigger'));
    </script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


</body>

</html>
