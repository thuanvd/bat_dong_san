<?php

namespace App\Http\Controllers;
use App\Http\Requests\AdminUserRequest;
use App\Role;
use App\User;
use App\Userstatus;
use Illuminate\Http\Request;

class AdminUserController extends Controller {
	public function index(Request $request) {
		$users = User::getuserinfo();
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		return view('admin.users', ['admin_infos' => $admin_infos,
			'users' => $users]);
	}
	public function destroy(Request $request, $id) {
		$user = User::findOrFail($id);
		$user->delete();
		return redirect('admin/users')->with('message', 'Delete success!');
	}
	public function create(Request $request) {

		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		$roles = Role::pluck('role_name', 'id');
		$status = Userstatus::pluck('status_name', 'id');
		return view('admin.create_user', ['admin_infos' => $admin_infos, 'roles' => $roles, 'status' => $status], compact($roles, $status))->with('message', 'Please type info!');
	}
	public function store(AdminUserRequest $request) {
		$data = $request->all();
		$user = new User;
		$user->name = $data['name'];
		$user->account = $data['account'];
		$user->password = $data['password'];
		$user->email = $data['email'];
		$user->role_id = $data['role_id'];
		$user->status_id = $data['status_id'];
		$user->save();
		return redirect('admin/users')->with('message', 'Add new user success!');

	}
	public function edit(Request $request, $id) {
		$user = User::find($id);
		$roles = Role::pluck('role_name', 'id');
		$status = Userstatus::pluck('status_name', 'id');
		$admin_id = $request->session()->get('id');
		$admin_infos = User::where('id', $admin_id)->get();
		return view('admin.edit_user', ['admin_infos' => $admin_infos, 'user' => $user, 'roles' => $roles, 'status' => $status], compact($roles), compact($status))->with('message', 'Please update!');
	}
	public function update(AdminUserRequest $request, $id) {
		$user = User::find($id);
		$data = $request->all();
		$user->name = $data['name'];
		$user->account = $data['account'];
		$user->password = $data['password'];
		$user->email = $data['email'];
		$user->role_id = $data['role_id'];
		$user->status_id = $data['status_id'];
		$user->save();
		return redirect('admin/users')->with('message', 'Update success!');

	}

}
