@extends('layouts.adminlayouts')
@section('content')
    <div class="simple-text"><h3>Categories</h3></div>

    @if(Session::has('message'))
        <p id="mess" style="display: none">
            {{Session::get('message')}}
        </p>
    @else
        <p id="mess" style="display: none">
            Welcome to Category Management!
        </p>
    @endif


        <div class="col-md-2"></div>
        <div class="col-md-6">
            {!!  Form::open(['method' => 'GET', 'route' => ['admin.categories.create']])  !!}
            {!!   Form::submit('Create New Category', ['class' => 'btn btn-warning']) !!}
            {!!  Form::close() !!}
            <br>
            <div class="row" align="center">


<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <th>ID</th>
            <th>NAME</th>
            <th>DELETE</th>
             <th>EDIT</th>
        </thead>
        <tfoot>
             <th>ID</th>
            <th>NAME</th>
            <th>DELETE</th>
             <th>EDIT</th>
        </tfoot>
        <tbody>
           
        
        @foreach($categs as $categ)
            <tr>
                <td>{{$categ->id}}</td>
                <td>{{$categ->categ_name}}</td>

                <td >
                    {!!  Form::open(['method' => 'DELETE', 'route' => ['admin.categories.destroy',$categ->id]])  !!}
                    {!!   Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!!  Form::close() !!}
                </td>
                <td>
                    {!!  Form::open(['method' => 'GET', 'route' => ['admin.categories.edit',$categ->id]])  !!}
                    {!!   Form::submit('Edit', ['class' => 'btn btn-info']) !!}
                    {!!  Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
        </div>
    </div>


@endsection